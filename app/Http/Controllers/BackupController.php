<?php

namespace App\Http\Controllers;
use Spatie\Backup\BackupServiceProvider;
use Alert;
use App\Http\Requests;
use Artisan;
use Log;
use Storage;
use App\vehicles;
use  App\owner;
use  App\drivers;
use App\Http\Controllers\HomeController;

class BackupController extends Controller
{
    public function index()
    {

    }

    public function create()
    {

            Artisan::call('backup:run');

        $vehicles=vehicles::all();
        $owners=owner::all();
        $drivers=drivers::all();
        $expired = HomeController::extract_expired_vehicles();
        $msg = "Backup success.";
      return view('index',compact('vehicles','expired','owners','drivers','msg'));




    }

    /**
     * Downloads a backup zip file.
     *
     * TODO: make it work no matter the flysystem driver (S3 Bucket, etc).
     */
    public function download($file_name)
    {

    }

    /**
     * Deletes a backup file.
     */
    public function delete($file_name)
    {

    }
}
