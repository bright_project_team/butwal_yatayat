<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\vehicles;
use App\owner;
use App\drivers;
use App\Tripfee;
use App\Tickets;
use App\Accident;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Session;
class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response



     */

     public function __construct()
     {
         $this->middleware('auth');
     }
    public function index()
    {
      $vehicles=vehicles::all();
        $expired = HomeController::extract_expired_vehicles();
        return view('vehicle/show',compact('vehicles','expired'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $expired = HomeController::extract_expired_vehicles();
      return view('vehicle/add',compact('expired'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $message= ['v_no.required'=>'Vehicle Number is Required',
                  'v_no.unique'=>'The vehicle number has already been taken',
                'v_company.required'=>'vehicle company name is Required',
                'v_model.required'=>'vehicle model name is Required',
                'v_type.required'=>'vehicle type  is Required',
                  'from.required'=>'vehicle Route from  is Required',
                      'to.required'=>'vehicle Route to  is Required',
                      'date_from.required'=>'vehicle insurance start date is required',
                      'date_to.required' =>'vehicle insurance end date is required',
                      'chassis_no.required' =>'vehicle chassis number  is required',
              ];
      $this->validate($request,[
       'v_no'=>'required|unique:vehicles|',
         'v_company'=>'required',
         'v_model'=>'required',
         'chassis_no'=>'required',
         'v_type'=>'required',
         'from'=>'required',
         'to'=>'required',
         'date_from'=>'required',
         'date_to'=>'required',
     ],$message);


      $vehicles= new vehicles;
      $vehicles->v_no = $request->v_no;
      $vehicles->company_name = $request->v_company;
      $vehicles->model =$request->v_model;
      $vehicles->chassis_no =$request->chassis_no;
      $vehicles->v_type=$request->v_type;
      $vehicles->route_from=$request->from;
      $vehicles->route_to=$request->to;
      $vehicles->date_from= date('Y-m-d',strtotime($request->date_from));
      $vehicles->date_to=date('Y-m-d',strtotime($request->date_to));

      $vehicles->save();

      Session::flash('message', 'New vehicle has been successfully created.');
      return redirect(route('owner.create'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $expired = HomeController::extract_expired_vehicles();
        $vehicle = vehicles::where('v_no', $id)->first();
        $owners = owner::all();
        foreach($owners as $owner){
            $v_no_array = unserialize($owner->v_no);
            foreach($v_no_array as $v_no){
                if($v_no == $id){
                    $owner_detail = [
                        'name' => $owner->name,
                        'address' => $owner->address,
                        'contact' => $owner->contact_no,
                    ];
                    break;
                }
            }
        }
        $drivers = drivers::all();
        foreach($drivers as $driver){
            $v_no_array = unserialize($driver->v_no);
            foreach($v_no_array as $v_no){
                if($v_no == $id){
                    $driver_detail[] = [
                        'card_no' => $driver->card_no,
                        'name' => $driver->name,
                        'address' => $driver->address,
                        'contact' => $driver->contact_no,
                        'license_no' => $driver->license_no,
                        'age' => $driver->age,
                        'gender' => $driver->gender,
                        'blood_group' => $driver->blood_group,
                        'experience' => $driver->experience,
                        'type' => $driver->type,
                        'card_expiry_date' => $driver->end_date,
                    ];
                    break;
                }
            }
        }
        $tripfees = Tripfee::where('v_no','=',$id)->get();
        $accidents = Accident::where('v_no','=',$id)->get();
        $tickets = Tickets::where('v_no','=',$id)->get();

        return view('vehicle/details',compact('expired','vehicle','owner_detail','driver_detail','tripfees','accidents','tickets'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $vehicles=vehicles::where('id', $id)->first();
        $expired = HomeController::extract_expired_vehicles();
        return view('vehicle.edit',compact('vehicles','expired'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $this->validate($request,[
         'v_no'=>'required',
           'v_company'=>'required',
           'v_model'=>'required',
           'chassis_no'=>'required',
           'v_type'=>'required',
           'from'=>'required',
           'to'=>'required',
        ]);


        $vehicles= vehicles::find($id);
        $vehicles->v_no = $request->v_no;
        $vehicles->company_name = $request->v_company;
        $vehicles->model =$request->v_model;
        $vehicles->chassis_no =$request->chassis_no;
        $vehicles->v_type=$request->v_type;
        $vehicles->route_from=$request->from;
        $vehicles->route_to=$request->to;
        $vehicles->date_from= date('Y-m-d',strtotime($request->date_from));
        $vehicles->date_to=date('Y-m-d',strtotime($request->date_to));
        $vehicles->save();

        Session::flash('message', 'Vehicle has been successfully updated.');
        return redirect(route('vehicle.index'));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $v = vehicles::select('v_no')->where('id',$id)->get();
        $owners = owner::all();
        foreach($owners as $owner){
            $v_no_array = unserialize($owner->v_no);
            for($i = 0; $i < count($v_no_array); $i++){
                if($v_no_array[$i] == $v[0]->v_no){
                    if(count($v_no_array) == 1){
                        $owner->delete();
                        break;
                    }else{
                        unset($v_no_array[$i]); // remove item at index 0
                        $v_no_array = array_values($v_no_array);
                        $owner->v_no = serialize($v_no_array);
                        $owner->save();
                        break;
                    }
                }
            }
        }

        vehicles::where('id', $id)->delete();
        drivers::where('v_no', $v[0]->v_no)->delete();
        Session::flash('flash_message','Vehicle has been successfully deleted.');
        return redirect()->back();
    }
}
