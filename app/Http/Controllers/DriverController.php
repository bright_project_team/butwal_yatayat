<?php

namespace App\Http\Controllers;

use App\drivers;
use App\vehicles;
use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Session;
use Illuminate\View\Middleware\ShareErrorsFromSession;

class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
     {
         $this->middleware('auth');
     }

    public function index()
    {
        $driver  = drivers::all();
        $expired = HomeController::extract_expired_vehicles();
        return view('driver.show', compact('driver','expired'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vehicles = vehicles::all();
        $drivers = drivers::all();
        $expired = HomeController::extract_expired_vehicles();
        return view('driver/add' , compact('vehicles','expired','drivers'));
      }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message =  [
            'v_number.required' =>'Vehicle number is required',
            'd_name.required' =>'Name field is required',
            'd_address.required'=>'Address field is required',
            'd_contact.required'=>'Contact field is required',
            'd_license_no.required'=>'License No field is required',
            'd_age.required'=>'Age field is required',
            'gender.required'=>'Gender field is required',
            'blood_group.required'=>'Blood Group field is required',
            'd_experience.required'=>'Experience field is required',
            'type.required'=>'Type field is required',
            'start_date.required'=>'Start Date field is required',
            'end_date.required'=>'End Date field is required',
            'card_no.required'=>'Card Number field is required',
        ];

        $this->validate($request,[
            'v_number'=>'required',
            'd_name'=>'required',
            'd_address'=>'required',
            'd_contact'=>'required',
            'd_license_no'=>'required',
            'd_age'=>'required',
            'gender'=>'required',
            'blood_group'=>'required',
            'd_experience'=>'required',
            'type'=>'required',
            'start_date'=>'required',
            'end_date'=>'required',
            'card_no'=>'required',
        ],$message);



        $vehicle_no = serialize($request['v_number']);
        $driver_name = $request['d_name'];
        $driver_address = $request['d_address'];
        $driver_contact = $request['d_contact'];
        $vehicle_license_no = $request['d_license_no'];
        $driver_age = $request['d_age'];
        $driver_gender = $request['gender'];
        $driver_blood_group = $request['blood_group'];
        $experience = $request['d_experience'];
        $type = $request['type'];
        $start_date = $request['start_date'];
        $end_date = $request['end_date'];
        $card_no = $request['card_no'];

        $driver = new drivers();

        $driver->v_no = $vehicle_no;
        $driver->name = $driver_name;
        $driver->address = $driver_address;
        $driver->contact_no = $driver_contact;
        $driver->license_no = $vehicle_license_no;
        $driver->age = $driver_age;
        $driver->gender = $driver_gender;
        $driver->blood_group = $driver_blood_group;
        $driver->experience = $experience;
        $driver->type = $type;
        $driver->start_date = $start_date;
        $driver->end_date = $end_date;
        $driver->card_no = $card_no;

        $driver->save();
        Session::flash('message', 'New driver has been successfully created.');
        return redirect()->route('manage');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vehicles = vehicles::all();
        $driver =  drivers::where('id' , $id)->first();
        $expired = HomeController::extract_expired_vehicles();
        return view('driver.edit',compact('driver','expired','vehicles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $driver = drivers::find($id);

        $driver->v_no = serialize($request->v_number);

        $driver->name = $request->d_name;
        $driver->address = $request->d_address;
        $driver->contact_no = $request->d_contact;
        $driver->license_no = $request->d_license_no;
        $driver->age = $request->d_age;
        $driver->gender = $request->gender;
        $driver->blood_group = $request->blood_group;
        $driver->experience = $request->d_experience;
        $driver->type = $request->type;
        $driver->start_date = $request->start_date;
        $driver->end_date = $request->end_date;
        $driver->card_no = $request->card_no;

        $driver->save();
        Session::flash('message', 'Driver has been successfully updated.');
        return redirect()->route('driver.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        drivers::where('id', $id)->delete();
        return redirect()->route('driver.index');
    }
}
