<?php

namespace App\Http\Controllers;
use App\Tripfee;
use App\vehicles;
use  App\owner;
use  App\drivers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Excel;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Facades\Redirect;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicles=vehicles::all();
        $owners=owner::all();
        $drivers=drivers::all();
        $expired = $this->extract_expired_vehicles();

        return view('index',compact('vehicles','expired','owners','drivers'));
    }

    public static function extract_expired_vehicles(){
        $compare_date = date('y-m-d',strtotime('+7 days'));
        $expired = vehicles::where('date_to','<=',$compare_date)->get();
        return $expired;
    }

    public function report(Request $request){

        $message= ['date_from.required'=>'From field is required',
            'date_to.required'=>'To field is required'
            ];

        $this->validate($request,[
         'date_from'=>'required',
            'date_to'=>'required',
        ],$message);



        $expired = $this->extract_expired_vehicles();
        $from = $request['date_from'];
        $to = $request['date_to'];


        session()->put('from', $from);
        session()->put('to', $to);

        $tripfee = DB::table('Tripfees')

            ->whereBetween('Tripfees.created_at', array($from, $to))
            ->get();

        $total = DB::table('Tripfees')
            ->select(DB::raw("SUM(amount_first + amount_second) as count"))
            ->whereBetween('Tripfees.created_at', array($from, $to))
            ->get();



        return view('report.tripfeereport',compact('tripfee','expired','from','to','total'));

    }


    public function export()
    {

        $from = session()->get('from');
        $to = session()->get('to');
        $tripfee = DB::table('tripfees')

            ->whereBetween('Tripfees.created_at', array($from, $to))
            ->select('tripfees.v_no', 'tripfees.t_type', 'tripfees.amount_first', 'tripfees.amount_second', 'tripfees.start_date', 'tripfees.end_date')
            ->get();


        if (count($tripfee)>0) {


            Excel::create('tripfee', function ($excel) {
                $excel->sheet('ExportFile', function ($sheet) {


                    $from = session()->get('from');
                    $to = session()->get('to');
                    $tripfee = DB::table('tripfees')

                        ->whereBetween('Tripfees.created_at', array($from, $to))
                        ->select('tripfees.v_no', 'tripfees.t_type', 'tripfees.amount_first', 'tripfees.amount_second', 'tripfees.start_date', 'tripfees.end_date')
                        ->get();

                    foreach ($tripfee as $tripfees) {
                        $owners = owner::all();
                        foreach ($owners as $owner) {
                            $v_no_array = unserialize($owner->v_no);
                            foreach ($v_no_array as $v_no) {
                                if ($v_no == $tripfees->v_no) {
                                    $owner_name = $owner->name;
                                    break;
                                }
                            }
                        }
                        $data[] = array(
                            $tripfees->v_no,
                            $tripfees->t_type,

                            'owner_name' => $owner_name,
                            $tripfees->amount_first,
                            $tripfees->amount_second,
                            'total' => $tripfees->amount_first + $tripfees->amount_second,
                            $tripfees->start_date,
                            $tripfees->end_date,

                        );
                    }

                    $sheet->fromArray($data, null, 'A1', false, false);
                    $sheet->prependRow(1, array(
                        'Vehicle Number', 'TripFee Type', 'Owner Name', 'Amount First', 'Amount Second', 'total', 'Start Date', 'End Date'
                    ));

                    $sheet->cell('A1:H1', function($cell) {

                        // manipulate the cell
                        $cell->setFontWeight('bold');

                    });

                });
            })->export('xls');
        }
        else {


            return redirect()->route('manage')->with('message', 'Can not export to the excel file. You have to search in valid date format.\'');
        }
    }

public function insuranceReport(Request $request){
    $message= ['insurance_from.required'=>'From field is required',
        'insurance_to.required'=>'To field is required'
    ];

    $this->validate($request,[
        'insurance_from'=>'required',
        'insurance_to'=>'required',
    ],$message);


    $expired = $this->extract_expired_vehicles();
    $i_from = $request['insurance_from'];
    $i_to = $request['insurance_to'];
    session()->put('i_from', $i_from);
    session()->put('i_to', $i_to);
    $insurance = DB::table('vehicles')



        ->whereBetween('vehicles.date_to', array($i_from, $i_to))
        ->get();

    return view('report.insurancereport',compact('insurance','expired','i_from','i_to'));
}



public function exportInsurance(){
    $i_from = session()->get('i_from');
    $i_to = session()->get('i_to');

    $insurance = DB::table('vehicles')


        ->whereBetween('vehicles.date_to', array($i_from, $i_to))
        ->get();


    if (count($insurance)>0) {


        Excel::create('insurance', function ($excel) {
            $excel->sheet('ExportFile', function ($sheet) {


                $i_from = session()->get('i_from');
                $i_to = session()->get('i_to');

                $insurance = DB::table('vehicles')



                    ->whereBetween('vehicles.date_to', array($i_from, $i_to))
                    ->get();


                foreach ($insurance as $datas) {
                    $owners = owner::all();
                    foreach ($owners as $owner) {
                        $v_no_array = unserialize($owner->v_no);
                        foreach ($v_no_array as $v_no) {
                            if ($v_no == $datas->v_no) {
                                $owner_name = $owner->name;
                                $owner_contact =$owner->contact_no;
                                break;
                            }
                        }
                    }
                    $data[] = array(
                        $datas->v_no,

                       "owner_name"=>$owner_name,
                       "owner_contact"=>$owner_contact,
                       $datas->date_to

                    );

                }

                $sheet->fromArray($data, null, 'A1', false, false);
                $sheet->prependRow(1, array(
                    'Vehicle Number',  'Owner Name','Owner Contact','Expire Date'
                ));
                $sheet->cell('A1:D1', function($cell) {

                    // manipulate the cell
                    $cell->setFontWeight('bold');

                });


            });
        })->export('xls');
    }
    else {

        return redirect()->route('manage')->with('message', 'Can not export to the excel file. You have to search in valid date format.');

    }
}

    public function ticketReport(Request $request){
        $message= ['ticket_from.required'=>'From field is required',
            'ticket_to.required'=>'To field is required'
        ];

        $this->validate($request,[
            'ticket_from'=>'required',
            'ticket_to'=>'required',
        ],$message);


        $expired = $this->extract_expired_vehicles();
        $t_from = $request['ticket_from'];
        $t_to = $request['ticket_to'];


        session()->put('t_from', $t_from);
        session()->put('t_to', $t_to);
        $sum = DB::table('tickets')
            ->select(DB::raw("SUM(amount) as count"))
            ->whereBetween('tickets.created_at', array($t_from, $t_to))
             ->get();


        $ticket  = DB::table('tickets')
          ->whereBetween('tickets.created_at', array($t_from, $t_to))
            ->get();

        return view('report.ticketreport',compact('ticket','expired','t_from','t_to','sum'));
    }


    public function ticketReportPrint(){
        $t_from = session()->get('t_from');
        $t_to = session()->get('t_to');


        $sum = DB::table('tickets')
            ->select(DB::raw("SUM(amount) as count"))
            ->whereBetween('tickets.created_at', array($t_from, $t_to))
            ->get();


        $ticket  = DB::table('tickets')
            ->whereBetween('tickets.created_at', array($t_from, $t_to))
            ->get();
        return view('ticket-report-print',compact('ticket','sum','t_from','t_to'));
    }

}
