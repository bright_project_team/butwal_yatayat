<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Tripfee;
use  App\owner;
use  App\drivers;
use Illuminate\Http\Request;
use App\vehicles;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Session;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Shankhadev\Bsdate\BsdateController;


class Tripfeecontroller extends BsdateController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
     {
         $this->middleware('auth');
     }
     
    public function index()
    {
        $tripfee  = tripfee::all();
        $expired = HomeController::extract_expired_vehicles();
        return view('tripfee.show', compact('tripfee','expired'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vehicle = vehicles::all();
        $expired = HomeController::extract_expired_vehicles();
       return view('tripfee.add', compact('vehicle','expired'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message =  [
            'v_number.required' =>'Vehicle number is required',

            't_type.required'=>'Trip Type field is required',
            'a_first.required'=>'Amount First field is required',
            'a_second.required'=>'Amount Second field is required',
            'date_from.required'=>'From date field is required',
            'date_to.required'=>'To date field is required',

        ];
        $this->validate($request,[
            'v_number'=>'required',

            't_type'=>'required',
            'a_first'=>'required',
            'a_second'=>'required',
            'date_from'=>'required',
            'date_to'=>'required',

        ],$message);

        $vehicle_no = $request['v_number'];

        $t_type = $request['t_type'];
        $a_first = $request['a_first'];
        $a_second = $request['a_second'];
        $date_from = $request['date_from'];
        $date_to = $request['date_to'];


        $trip = new Tripfee();

        $trip->v_no = $vehicle_no;

        $trip->t_type = $t_type;
        $trip->amount_first = $a_first;
        $trip->amount_second = $a_second;
        $trip->start_date = $date_from;
        $trip->end_date = $date_to;


        $trip->save();
        Session::flash('message', 'New tripfee has been successfully added.');
        return redirect()->route('tripfee.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tripfees=Tripfee::find($id);
        $owners=owner::all();
        $drivers=drivers::all();
        $np_date=$this->eng_to_nep($tripfees->created_at->format('Y'),$tripfees->created_at->format('m'),$tripfees->created_at->format('d'));
        $tripfee = array(
            'id' => $tripfees->id,

            'date_and_time_np' => $np_date['year'].'-'.$np_date['month'].'-'.$np_date['date'],
           'date_and_time' => $tripfees->created_at->format('Y-m-d'),
            'amount_first' => $tripfees->amount_first,
            'amount_second' => $tripfees->amount_second,
            'v_no' => $tripfees->v_no,
            'type' => $tripfees->t_type,
            'start_date' => $tripfees->start_date,
            'end_date' => $tripfees->end_date,
            'total' => $tripfees->amount_first + $tripfees->amount_second,
        );

        foreach ($drivers as $d){
            $v_no_array = unserialize($d->v_no);
            foreach($v_no_array as $v_no){
                if($v_no == $tripfees->v_no){
                    $driver = array(
                        'name' => $d->name,
                        'address' => $d->address,
                        'contact_no' => $d->contact_no,
                        'license_no' => $d->license_no,
                    );
                }
            }
        }
        foreach ($owners as $o){
            $v_no_array = unserialize($o->v_no);
            foreach($v_no_array as $v_no){
                if($v_no == $tripfees->v_no){
                    $owner = array(
                        'name' => $o->name,
                        'address' => $o->address,
                        'contact_no' => $o->contact_no,
                        'license_no' => $o->license_no,
                    );
                }
            }
        }

        return view('print-tripfee-receipt', compact('tripfee','owner','driver'));



    }




    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//        $vehicles = vehicles::all();
//        $details = Tripfee::where('id', $id)->first();
//        $expired = HomeController::extract_expired_vehicles();
//        return view('tripfee.edit',compact('details','expired','vehicles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        $message =  [
//            'v_number.required' =>'Vehicle number is required',
//
//            't_type.required'=>'Trip Type field is required',
//            'a_first.required'=>'Amount First field is required',
//            'a_second.required'=>'Amount Second field is required',
//            'date_from.required'=>'From date field is required',
//            'date_to.required'=>'To date field is required',
//
//        ];
//        $this->validate($request,[
//            'v_number'=>'required',
//
//            't_type'=>'required',
//            'a_first'=>'required',
//            'a_second'=>'required',
//            'date_from'=>'required',
//            'date_to'=>'required',
//
//        ],$message);
//
//        $vehicle_no = $request['v_number'];
//
//        $t_type = $request['t_type'];
//        $a_first = $request['a_first'];
//        $a_second = $request['a_second'];
//        $date_from = $request['date_from'];
//        $date_to = $request['date_to'];
//
//
//        $trip = Tripfee::find($id);
//
//        $trip->v_no = $vehicle_no;
//
//        $trip->t_type = $t_type;
//        $trip->amount_first = $a_first;
//        $trip->amount_second = $a_second;
//        $trip->start_date = $date_from;
//        $trip->end_date = $date_to;
//
//
//        $trip->save();
//        Session::flash('message', 'Tripfee has been successfully updated.');
//        return redirect()->route('tripfee.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Tripfee::where('id', $id)->delete();
        return redirect()->back();
    }
}
