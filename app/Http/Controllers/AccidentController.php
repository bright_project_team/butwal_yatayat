<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Accident;
use App\vehicles;
use App\drivers;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Session;
use Illuminate\View\Middleware\ShareErrorsFromSession;

class AccidentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
     {
         $this->middleware('auth');
     }
     
    public function index()
    {

        $accidents = Accident::all();
        $expired = HomeController::extract_expired_vehicles();
        return view('accident.show',compact('accidents','expired'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vehicles = vehicles::all();
        $drivers = drivers::all();
        $expired = HomeController::extract_expired_vehicles();
        return view('accident.add',compact('vehicles','drivers','expired'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



        $old_date_timestamp = strtotime($request->date);
        $request->date = date('Y-m-d', $old_date_timestamp);

        $message= [
            'v_no.required'=>'Vehicle Number is Required.',
            'd_name.required'=>'Driver\'s name is Required.',
            'date.required'=>'Accident date is Required.',
            'location.required'=>'Accident location is Required.',
            'type.required'=>'Accident type is Required.',
            'cost_bearer.required'=>'Cost bearer is Required.',
        ];

        $this->validate($request,[
            'v_no'=>'required',
            'd_name'=>'required',
            'date'=>'required',
            'location'=>'required',
            'type'=>'required',
            'cost_bearer'=>'required',
        ],$message);

        $accident= new Accident;
        $accident->v_no = $request->v_no;
        $accident->D_name = $request->d_name;
        $accident->date = $request->date;
        $accident->location = $request->location;
        $accident->type = $request->type;
        $accident->details = $request->details;
        $accident->cost_bearer = $request->cost_bearer;

        $accident->save();

        Session::flash('message', 'New accident has been successfully created.');
        return redirect(route('accident.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vehicles = vehicles::all();
        $drivers = drivers::all();
        $details = Accident::where('id', $id)->first();
        $expired = HomeController::extract_expired_vehicles();
        return view('accident.edit',compact('vehicles','drivers','details','expired'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $old_date_timestamp = strtotime($request->date);
        $request->date = date('Y-m-d', $old_date_timestamp);

        $message= [
            'v_no.required'=>'Vehicle Number is Required.',
            'd_name.required'=>'Driver\'s name is Required.',
            'date.required'=>'Accident date is Required.',
            'location.required'=>'Accident location is Required.',
            'type.required'=>'Accident type is Required.',
            'cost_bearer.required'=>'Accident type is Required.',
        ];

        $this->validate($request,[
            'v_no'=>'required',
            'd_name'=>'required',
            'date'=>'required',
            'location'=>'required',
            'type'=>'required',
            'cost_bearer'=>'required',
        ],$message);

        $accident = Accident::find($id);
        $accident->v_no = $request->v_no;
        $accident->D_name = $request->d_name;
        $accident->date = $request->date;
        $accident->location = $request->location;
        $accident->type = $request->type;
        $accident->cost_bearer = $request->cost_bearer;
        $accident->details = $request->details;

        $accident->save();

        Session::flash('message', 'Accident has been successfully updated.');
        return redirect(route('accident.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        accident::where('id', $id)->delete();
        return redirect()->back();
    }
}
