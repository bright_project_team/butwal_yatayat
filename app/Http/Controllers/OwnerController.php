<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\owner;
use App\vehicles;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Session;
use Illuminate\View\Middleware\ShareErrorsFromSession;

class OwnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


     public function __construct()
     {
         $this->middleware('auth');
     }

    public function index()
    {
        $owners=owner::all();
        $expired = HomeController::extract_expired_vehicles();
        return view('owner.show',compact('owners','expired'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vehicles = vehicles::all();
        $owners = owner::all();
        foreach($vehicles as $vehicle){
            $owned = FALSE;
            foreach($owners as $owner){
                $v_no_array = unserialize($owner->v_no);
                foreach($v_no_array as $v_no){
                    if($v_no == $vehicle->v_no){
                        $owned = TRUE;
                    }
                }
            }
            if(!$owned){
                $unowned_vehicles[] = $vehicle->v_no;
            }
        }
        $expired = HomeController::extract_expired_vehicles();
        return view('owner.add',compact('unowned_vehicles','expired'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $message= [
            'v_no.required'=>'Vehicle Number is Required.',
            'o_name.required'=>'Owner\'s name is Required.',
            'o_address.required'=>'Owner\'s address is Required.',
            'o_contact.required'=>'Owner\'s contact is Required.',
            'insurance.required'=>'Owner\'s insurance field is Required.',
        ];

        $this->validate($request,[
            'v_no'=>'required',
            'o_name'=>'required',
            'o_address'=>'required',
            'o_contact'=>'required',
            'insurance'=>'required',
        ],$message);

        $request->v_no = serialize($request->v_no);

        $owner= new owner;
        $owner->v_no = $request->v_no;
        $owner->name = $request->o_name;
        $owner->address = $request->o_address;
        $owner->contact_no = $request->o_contact;
        $owner->insurance = $request->insurance;

        $owner->save();

        Session::flash('message', 'New owner has been successfully created.');
        return redirect(route('driver.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vehicles = vehicles::all();
        $owners = owner::all();
        foreach($vehicles as $vehicle){
            $owned = FALSE;
            foreach($owners as $owner){
                $v_no_array = unserialize($owner->v_no);
                foreach($v_no_array as $v_no){
                    if($v_no == $vehicle->v_no){
                        $owned = TRUE;
                    }
                }
            }
            if(!$owned){
                $unowned_vehicles[] = $vehicle->v_no;
            }
        }
        $details = owner::where('id', $id)->first();
        $details->v_no = unserialize($details->v_no);
        $expired = HomeController::extract_expired_vehicles();
        return view('owner.edit',compact('details','unowned_vehicles','expired'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $message= [
            'v_no.required'=>'Vehicle Number is Required.',
            'o_name.required'=>'Owner\'s name is Required.',
            'o_address.required'=>'Owner\'s address is Required.',
            'o_contact.required'=>'Owner\'s contact is Required.',
            'insurance.required'=>'Owner\'s insurance field is Required.',
        ];

        $this->validate($request,[
            'v_no'=>'required',
            'o_name'=>'required',
            'o_address'=>'required',
            'o_contact'=>'required',
            'insurance'=>'required',
        ],$message);

        $request->v_no = serialize($request->v_no);

        $owner = owner::find($id);
        $owner->v_no = $request->v_no;
        $owner->name = $request->o_name;
        $owner->address =$request->o_address;
        $owner->contact_no=$request->o_contact;
        $owner->insurance=$request->insurance;

        $owner->save();

        Session::flash('message', 'Owner details has been successfully updated.');
        return redirect(route('owner.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        owner::where('id', $id)->delete();
        return redirect()->back();
    }
}
