<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\vehicles;
use App\Tickets;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\View\Middleware\ShareErrorsFromSession;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
     {
         $this->middleware('auth');
     }
     
    public function index()
    {
        $tickets = Tickets::all();
        $expired = HomeController::extract_expired_vehicles();
        return view('ticket.show',compact('tickets','expired'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vehicles = vehicles::all();
        $tickets = DB::table('tickets')
            ->select('particular')
            ->groupBy('particular')
            ->get();

        $expired = HomeController::extract_expired_vehicles();
        return view('ticket.add',compact('vehicles','expired','tickets'));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $message= [
           /* 'v_no.required'=>'Vehicle Number is Required.',*/
            'particular.required'=>'Particular is Required.',
            'name.required'=>'Name is required',
            'date.required'=>'Date is Required.',
            'amount.required'=>'Amount is Required.',
        ];

        $this->validate($request,[
          /*  'v_no'=>'required',*/
            'particular'=>'required',
            'name'=>'required',
            'date'=>'required',
            'amount'=>'required',
        ],$message);

        $ticket = new Tickets;
        $ticket->v_no = $request->v_no;
        $ticket->particular = $request->particular;
        $ticket->name = $request->name;
        $ticket->date = $request->date;
        $ticket->amount = $request->amount;



        $ticket->save();

        Session::flash('message', 'New ticket has been successfully created.');
        return redirect(route('ticket.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ticket = Tickets::where('id','=',$id)->first();

        return view('print-ticket-receipt', compact('ticket'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vehicles = vehicles::all();
        $tickets = Tickets::all();
        $details = Tickets::where('id', $id)->first();
        $expired = HomeController::extract_expired_vehicles();
        return view('ticket.edit',compact('vehicles','details','expired','tickets'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $message= [
          /*  'particular.required'=>'Particular is Required.',*/
            'name.required'=>'Name is required',
            'date.required'=>'Date is Required.',
            'amount.required'=>'Amount is Required.',
        ];

        $this->validate($request,[
            /*'v_no'=>'required',*/
            'particular'=>'required',
            'name'=>'required',
            'date'=>'required',
            'amount'=>'required',
        ],$message);

        $ticket = Tickets::find($id);
        $ticket->v_no = $request->v_no;
        $ticket->particular = $request->particular;
        $ticket->name = $request->name;
        $ticket->date = $request->date;
        $ticket->amount = $request->amount;

        $ticket->save();

        Session::flash('message', 'Ticket details has been successfully updated.');
        return redirect(route('ticket.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Tickets::where('id', $id)->delete();
        return redirect()->back();
    }
}
