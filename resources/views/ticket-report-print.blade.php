<!DOCTYPE html>

<html lang="en-US">

<head>
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}" />
</head>

<body>
<div class="container" >
    <main>
        <div class="sec1">
            <p class="left"><span class="bold">रजिष्टड</span> 101/091/092</p>
            <p class="right"><span class="bold">फोन नं.</span> 071-541105</p>
        </div>
        <div class="sec2">
            <div class="img-sec">
                <img class="logo-img" src="{{ asset('images/new_butwal_yatayat_logo.png') }}" alt="Butwal Yatayat" />
            </div>
            <div class="sec3">
                <h1>बुटवल यातायात व्यवसायि समिति</h1>
                <p class="small">प्रधान कार्यलय बुटवल-७</p>
                <p class="small">नगद रसिद</p>
            </div>
            <div class="sec4">
                <p>रसिद नं:     12345</p>
                <p>मिति:           {{date('Y-m-d')}}</p>

            </div>
        </div>
        <h4 style="text-align: center;">Report of Ticket Between <span style="font-size: 16px;color: blue;"> {{$t_from}}</span> and <span style="font-size: 16px;color: blue;"> {{$t_to}}</span> </h4>
        <div class="sec8" style="margin-bottom: 10px;">
            <table>
                <thead>
                <tr>
                    <th>SN</th>
                    <th>Vehicle No</th>
                    <th>Name</th>
                    <th>Particular</th>
                    <th>Amount</th>
                    <th>Date</th>


                </tr>
                </thead>
                <tbody>

                @foreach($ticket as $data)

                    <tr>
                        <td>{{$loop->index+1}}</td>
                        <td>@php if(!empty($data->v_no)) {echo $data->v_no;} else {echo '-';} @endphp</td>

                        <td>{{$data->name}}</td>
                        <td>{{$data->particular}}</td>
                        <td>{{$data->amount}}</td>
                        <td>{{date('Y-m-d',strtotime($data->created_at))  }} </td>

                    </tr>
                @endforeach
                </tbody>
                <?php if(count($ticket)>0){?>
                <tfoot>
                <td></td><td></td><td></td><td></td>
                <td>
                    @foreach($sum as $data)
                        Total: {{$data->count}}
                    @endforeach
                </td>
                <td></td>
                </tfoot>
                <?php }?>

            </table>
        </div>
    </main>

    <button type="button" onclick="window.print()" class="btn btn-default print-btn">print</button>

</div>
</body>
<style>


</style>
</html>
