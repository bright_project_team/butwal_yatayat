@extends("../layout/master")
@include("layout.footer")
@include("layout.header")
@include("layout.meta")
@include("layout.side-bar")

@section("main-content")
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Drivers</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Driver</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                    {{--<div class="box-header">
                        <h3 class="box-title">Data Table With Full Features</h3>
                    </div>--}}
                    <!-- /.box-header -->
                        <div class="box-body scrollable-table">
                            @if(Session::has('message'))
                                <p class="message"></p>
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h4><i class="icon fa fa-check"></i> Success</h4>
                                    {{ Session::get('message') }}
                                </div>
                            @endif

                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>S.N</th>
                                    <th>Vehicle Number</th>
                                    <th> Name</th>
                                    <th>Address</th>
                                    <th>Contact</th>
                                    <th>License No</th>
                                    <th>Age</th>
                                    <th>Blood Group</th>
                                    <th>Gender</th>
                                    <th>Experience</th>
                                    <th>Date</th>
                                    <th>Type</th>
                                    <th>Card No.</th>
                                    <th>Expiry Date</th>
                                    <th>Updated Date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                @forelse($driver as $data)
                                    <tr>

                                        <td>{{$loop->index+1 }}</td>
                                        <td>{{implode(', ',unserialize($data->v_no))}}</td>
                                        <td>{{$data->name}}</td>
                                        <td>{{$data->address}}</td>
                                        <td>{{$data->contact_no}}</td>
                                        <td>{{$data->license_no}}</td>
                                        <td>{{$data->age}}</td>
                                        <td>{{$data->blood_group}}</td>
                                        <td>{{$data->gender}}</td>
                                        <td>{{$data->experience}}</td>
                                        <td>{{\Carbon\Carbon::parse($data->creted_at)->format('Y-m-d')}}</td>
                                        <td>{{$data->type}}</td>
                                        <td>{{$data->card_no}}</td>
                                        <td>{{$data->end_date}}</td>
                                        <td>{{$data->updated_at}}</td>
                                        <td>
                                          @php $type= Auth::user()->user_type; @endphp
                                          @if($type=='admin')
                                            <a href="{{ route('driver.edit',$data->id) }}">   <i class="fa fa-fw fa-edit"></i></a>||

                                            <form id="delete-item{{$data->id}}"  action="{{route('driver.destroy',$data->id)}}" style="Display:none"  method="post">
                                                {{csrf_field()}}
                                                {{method_field('DELETE')}}

                                            </form>
                                            <a href="" onclick="
                                                    if (confirm('Are you sure, You want to delete this ?'))
                                                    {
                                                    event.preventDefault();
                                                    document.getElementById('delete-item{{$data->id}}').submit();
                                                    }
                                                    else{
                                                    event.preventDefault();
                                                    }

                                                    ">

                                                <i class="fa fa-fw fa-trash"></i></a>

                                                @endif
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        No data Are Found
                                    </tr>
                                @endforelse
                                </tbody>
                                {{--<tfoot>
                                <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                                </tr>
                                </tfoot>--}}
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
    </div>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
@endsection
