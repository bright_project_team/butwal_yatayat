@extends("../layout/master")
@include("layout.footer")
@include("layout.header")
@include("layout.meta")
@include("layout.side-bar")

@section("main-content")


    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <section class="content-header">
            <h1>Edit driver details</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Driver</li>
                <li class="active">Edit</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <form action="{{ route('driver.update',$driver->id) }}" method="POST">
                        {{ method_field('PATCH')}}


                        <div class="form-group">
                            <label>Vehicle Number</label>
                            <select class="form-control select2" multiple="multiple" style="width: 100%;" name="v_number[]">
                                <?php $driver->v_no = unserialize($driver->v_no); ?>
                                @forelse($vehicles as $vehicle)
                                    @foreach($driver->v_no as $v_no)
                                        @if($v_no === $vehicle->v_no)
                                            <option selected value="{{ $vehicle->v_no }}">{{ $vehicle->v_no }}</option>
                                        @else
                                            <option value="{{ $vehicle->v_no }}">{{ $vehicle->v_no }}</option>
                                        @endif
                                    @endforeach
                                @empty
                                    <option>Not Found</option>
                                @endforelse
                            </select>
                        </div>




                        <div class="form-group">
                            <label for="driver-name">Name</label>
                            <input id="driver-name" value="{{$driver->name}}" class="form-control" type="text" name="d_name" placeholder="Eg. Ram Bahadur" />
                        </div>
                        <div class="form-group">
                            <label for="driver-address">Address</label>
                            <input id="driver-address" value="{{$driver->address}}" class="form-control" type="text" name="d_address" placeholder="Eg. Butwal-10" />
                        </div>
                        <div class="form-group">
                            <label for="driver-contact">Contact No.</label>
                            <input id="driver-contact" value="{{$driver->contact_no}}" class="form-control" type="text" name="d_contact" placeholder="Eg. 9811111111" />
                        </div>
                        <div class="form-group">
                            <label for="driver-license-no">License No.</label>
                            <input id="driver-license-no" value="{{$driver->license_no}}" class="form-control" type="text" name="d_license_no" placeholder="Eg. 8231/123" />
                        </div>
                        <div class="form-group">
                            <label for="driver-age">Age (years)</label>
                            <input id="driver-age" value="{{$driver->age}}" class="form-control" type="text" name="d_age" placeholder="Eg. 30" />
                        </div>
                        <div class="form-group">
                            <label>Gender</label><br />
                            <label>Male
                                <input type="radio" name="gender" value="male" class="minimal"{{ $driver->gender == 'male' ? 'checked' : '' }}>
                            </label>
                            <label>
                                Female
                                <input type="radio" name="gender" value="female" class="minimal" {{ $driver->gender == 'female' ? 'checked' : '' }}>
                            </label>
                        </div>
                        <div class="form-group">
                            <label>Blood Group</label>
                            <select class="form-control" name="blood_group" id="blood-group">
                                <option {{ $driver->blood_group == 'A' ? 'selected' : '' }}>A</option>
                                <option {{ $driver->blood_group == 'B' ? 'selected' : '' }}>B</option>
                                <option  {{ $driver->blood_group == 'AB' ? 'selected' : '' }}>AB</option>
                                <option {{ $driver->blood_group == 'O' ? 'selected' : '' }}>O</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="driver-experience">Experience (years)</label>
                            <input id="driver-experience" value="{{$driver->experience}}" class="form-control" type="text" name="d_experience" placeholder="Eg. 10" />
                        </div>
                        <div class="form-group">
                            <label>Type<span class="astric">*</span></label>
                            @if ($errors->has('type'))
                                <span class="error">{{ $errors->first('type') }}</span>
                            @endif
                            <select class="form-control" name="type" id="type">
                                <option value="">Select One</option>
                                @if($driver->type === 'jeep')
                                <option selected value="jeep">Jeep</option>
                                <option value="bus">Bus</option>
                                <option value="truck">Truck</option>
                                @endif
                                @if($driver->type === 'bus')
                                    <option value="jeep">Jeep</option>
                                    <option selected value="bus">Bus</option>
                                    <option value="truck">Truck</option>
                                @endif
                                @if($driver->type === 'truck')
                                    <option selected value="jeep">Jeep</option>
                                    <option value="bus">Bus</option>
                                    <option selected value="truck">Truck</option>
                                @endif
                            </select>
                        </div>
                        <div class="form-horizontal row">
                            <label class="col-lg-12">Driver Card Validity</label>
                            <div class="form-group col-md-6">
                                <label class="col-sm-3 control-label">Start Date<span class="astrict">*</span> </label>
                                @if($errors->has('start_date'))
                                    <span class="error">  {{  $errors->first('start_date')}}</span>
                                @endif
                                <div class="input-group date">

                                    <div class="input-group-addon" >
                                        <i class="fa fa-calendar"></i>
                                    </div>

                                    <input type="text" class="form-control pull-right" id="from" name="start_date" value="{{ $driver->start_date }}">

                                </div>
                                <!-- /.input group -->
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-sm-3 control-label">End Date<span class="astrict">*</span> </label>
                                @if($errors->has('end_date'))
                                    <span class="error">  {{  $errors->first('end_date')}}</span>
                                @endif
                                <div class="input-group date">

                                    <div class="input-group-addon" >
                                        <i class="fa fa-calendar"></i>
                                    </div>

                                    <input type="text" class="form-control pull-right" id="to" name="end_date" value="{{ $driver->end_date }}">

                                </div>
                                <!-- /.input group -->
                            </div>

                        </div>
                        <div class="form-group">
                            <label for="card_no">Card No.<span class="astric">*</span></label>
                            @if ($errors->has('card_no'))
                                <span class="error">{{ $errors->first('card_no') }}</span>
                            @endif
                            <input id="card_no" class="form-control" type="text" name="card_no" placeholder="Eg. 1234" value="{{ $driver->card_no }}">
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" name="submit-vehicle" value="Submit" />
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2();

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
            //Datemask2 mm/dd/yyyy
            $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' });
            //Money Euro
            $('[data-mask]').inputmask();

            //Date range picker
            $('#reservation').daterangepicker();
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' });
            //Date range as a button
            $('#daterange-btn').daterangepicker(
                {
                    ranges   : {
                        'Today'       : [moment(), moment()],
                        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate  : moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
                }
            );

            //Date picker
            $('#datepicker').datepicker({
                autoclose: true
            });

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
            });
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass   : 'iradio_minimal-red'
            });
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass   : 'iradio_flat-green'
            });

            //Colorpicker
            $('.my-colorpicker1').colorpicker();
            //color picker with addon
            $('.my-colorpicker2').colorpicker();

            //Timepicker
            $('.timepicker').timepicker({
                showInputs: false
            });
        })
    </script>


@endsection