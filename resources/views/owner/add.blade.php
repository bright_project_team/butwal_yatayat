@extends("../layout/master")
@include("layout.footer")
@include("layout.header")
@include("layout.meta")
@include("layout.side-bar")

@section("main-content")

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add new owner
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Owner</a></li>
                <li><a href="#">Add</a></li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                    {{--<div class="box-header">
                        <h3 class="box-title">Data Table With Full Features</h3>
                    </div>--}}
                    <!-- /.box-header -->
                        <div class="box-body">
            <form action="{{route('owner.store')}}" method="POST">
                <div class="form-group">
                    <label>Vehicle Number</label>

                    @if($errors->has('v_no'))
                        <span class="error">  *{{  $errors->first('v_no')}}</span>
                    @endif

                    <select name="v_no[]" class="form-control select2" multiple="multiple"  data-placeholder="Select a Vehicle"
                            style="width: 100%;">
                        <option >Select Vehicle No</option>@if(isset($unowned_vehicles))
                        @forelse($unowned_vehicles as $vehicle)
                            <option >{{$vehicle}}</option>
                        @empty
                            <option>Not Found</option>
                        @endforelse @endif
                    </select>
                </div>
                <div class="form-group">
                    <label for="owner-name">Name</label>
                    @if($errors->has('o_name'))
                        <span class="error">  *{{  $errors->first('o_name')}}</span>
                    @endif
                    <input id="owner-name" class="form-control" type="text" name="o_name" placeholder="Eg. Ram Bahadur" />
                </div>
                <div class="form-group">
                    <label for="owner-address">Address</label>
                    @if($errors->has('o_address'))
                        <span class="error">  *{{  $errors->first('o_address')}}</span>
                    @endif
                    <input id="owner-address" class="form-control" type="text" name="o_address" placeholder="Eg. Butwal-10" />
                </div>
                <div class="form-group">
                    <label for="owner-contact">Contact No.</label>
                    @if($errors->has('o_contact'))
                        <span class="error">  *{{  $errors->first('o_contact')}}</span>
                    @endif
                    <input id="owner-contact" class="form-control" type="text" name="o_contact" placeholder="Eg. 9811111111" />
                </div>
                <div class="form-group">
                    <label for="owner-contact">Insurance</label>
                    <label>
                        <input type="radio" name="insurance" value="1" class="minimal" checked>
                        Yes
                    </label>
                    <label>
                        <input type="radio" name="insurance" value="0" class="minimal">
                        No
                    </label>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" name="submit-owner" value="Submit" />
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </div>
            </form>
                        </div></div></div></div>
        </section>
    </div>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2();

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
            //Datemask2 mm/dd/yyyy
            $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' });
            //Money Euro
            $('[data-mask]').inputmask();

            //Date range picker
            $('#reservation').daterangepicker();
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' });
            //Date range as a button
            $('#daterange-btn').daterangepicker(
                {
                    ranges   : {
                        'Today'       : [moment(), moment()],
                        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate  : moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
                }
            );

            //Date picker
            $('#datepicker').datepicker({
                autoclose: true
            });

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
            });
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass   : 'iradio_minimal-red'
            });
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass   : 'iradio_flat-green'
            });

            //Colorpicker
            $('.my-colorpicker1').colorpicker();
            //color picker with addon
            $('.my-colorpicker2').colorpicker();

            //Timepicker
            $('.timepicker').timepicker({
                showInputs: false
            });
        })
    </script>

@endsection
