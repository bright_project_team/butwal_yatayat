<!DOCTYPE html>

<?php
function convert_number_to_words($number) {

    $hyphen      = '-';
    $conjunction = ' and ';
    $separator   = ', ';
    $negative    = 'negative ';
    $decimal     = ' point ';
    $dictionary  = array(
        0                   => 'शुन्य',
        1                   => 'एक',
        2                   => 'दुइ',
        3                   => 'तीन',
        4                   => 'चार',
        5                   => 'पाच ',
        6                   => 'छ',
        7                   => 'सात',
        8                   => 'आठ',
        9                   => 'नौ',
        10                  => 'दस',
        11                  => 'यघार',
        12                  => 'बार',
        13                  => 'तेर',
        14                  => 'चौध',
        15                  => 'पन्ध्र',
        16                  => 'सोर',
        17                  => 'सत्र',
        18                  => 'अठार',
        19                  => 'उन्नाइस ',
        20                  => 'बिस',
        30                  => 'तिस',
        40                  => 'चालिस',
        50                  => 'पचास',
        60                  => 'साठी',
        70                  => 'सत्तरी',
        80                  => 'असि',
        90                  => 'नब्य',
        100                 => 'सय',
        1000                => 'हजार',
        1000000             => 'लाख',
        1000000000          => 'करोड'

    );

    if (!is_numeric($number)) {
        return false;
    }

    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
            'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
            E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
    }

    $string = $fraction = null;

    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }

    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens   = ((int) ($number / 10)) * 10;
            $units  = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds  = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= convert_number_to_words($remainder);
            }
            break;
    }

    if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }

    return $string;
}
?>

<html lang="en-US">

<head>
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}" />
</head>

<body>
<div class="container">
    <main class="ticket">
        <div class="sec1">
            <p class="left"><span class="bold">रजिष्टड</span> 101/091/092</p>
            <p class="right"><span class="bold">फोन नं.</span> 071-541105</p>
        </div>
        <div class="sec2 ticket">
            <div class="img-sec">
                <img class="logo-img" src="{{ asset('images/new_butwal_yatayat_logo.png') }}" alt="Butwal Yatayat" />
            </div>
            <div class="sec3">
                <h1>बुटवल यातायात व्यवसायि समिति</h1>
                <p class="small">प्रधान कार्यलय बुटवल-७</p>
                <p class="small">नगद रसिद</p>
            </div>
            <div class="sec4">
                <p>रसिद नं:         {{ $ticket->id }}</p>
                <p>मिति:           {{$ticket->date}}</p>
            </div>
        </div>
        <div class="sec6 ticket">
            <?php if(!empty( $ticket->v_no)){echo 'गाडी नं/श्री:';} else {echo 'श्री:';} ?>  <?php if(!empty( $ticket->v_no)){echo $ticket->v_no.'/'.$ticket->name;} else {echo $ticket->name;}?>  &nbsp;&nbsp;&nbsp;&nbsp;बाट {{ $ticket->particular }}  &nbsp;&nbsp;&nbsp;&nbsp;
          बापत रु .  {{ $ticket->amount }}  &nbsp;&nbsp;&nbsp;&nbsp;
            अक्षरुपी .

<?php
$t_no=$ticket->amount;
$string_number=convert_number_to_words($t_no);

?>

              {{$string_number}} &nbsp;&nbsp;
                सधन्यवाद प्राप्त भयो ।



        </div>

    </main>
    <footer>
        <div class="sec11 ticket">
            <p>.....................</p>
            <p><span class="bold">बुझ्नेको सहि</span></p>
        </div>
    </footer>


    <button type="button" onclick="window.print()" class="btn btn-default print-btn">Print</button>

</div>
</body>

</html>
