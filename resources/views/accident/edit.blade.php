@extends("../layout/master")
@include("layout.footer")
@include("layout.header")
@include("layout.meta")
@include("layout.side-bar")

@section("main-content")


    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Edit accident details</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Accident</li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                    {{--<div class="box-header">
                        <h3 class="box-title">Data Table With Full Features</h3>
                    </div>--}}
                    <!-- /.box-header -->
                        <div class="box-body">
                            <form action="{{route('accident.update',$details["id"])}}" method="POST" name="form">
                                {{ csrf_field()}}
                                {{ method_field('PATCH')}}

                                <div class="form-group" >
                                    <label for="v_number">Vehicle Number<span class="astric">*</span></label>
                                    @if ($errors->has('v_no'))
                                        <span class="error">{{ $errors->first('v_no') }}</span>
                                    @endif
                                    <select class="form-control select2" name="v_no" id="v_number" >
                                        style="width: 100%;">
                                        @foreach($vehicles as $vehicle)
                                                @if($details->v_no === $vehicle->v_no)
                                                    <option selected>{{ $vehicle->v_no }}</option>
                                                @else
                                                    <option>{{ $vehicle->v_no }}</option>
                                                @endif
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group" >
                                    <label>Driver Name<span class="astric">*</span></label>
                                    @if ($errors->has('d_name'))

                                        <span class="error">{{ $errors->first('d_name') }}</span>
                                    @endif
                                    <select class="form-control select2" name="d_name" id="d_name">
                                        style="width: 100%;">
                                        <option value="" disabled selected>Driver Name</option>
                                        @foreach($drivers as $driver)
                                            @if($details->D_name === $driver->name)
                                                <option selected>{{ $details->D_name }}</option>
                                            @else
                                                <option>{{ $details->D_name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="location">Location<span class="astric">*</span></label>
                                    @if ($errors->has('location'))
                                        <span class="error">{{ $errors->first('location') }}</span>
                                    @endif
                                    <input id="location" class="form-control" type="text" name="location" value="{{ $details->location }}" />
                                </div>

                                <div class="form-group">
                                    <label for="date">Date<span class="astric">*</span> </label>
                                    @if ($errors->has('date'))
                                        <span class="error">{{ $errors->first('date') }}</span>
                                    @endif
                                    <div class="input-group date">

                                        <div class="input-group-addon" >
                                            <i class="fa fa-calendar"></i>
                                        </div>

                                        <input type="text" class="form-control pull-right" id="to" name="date" value="{{ $details->date }}"  />

                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <div class="form-group">
                                    <label for="cost_bearer">Cost Bearer<span class="astric">*</span></label>
                                    @if ($errors->has('cost_bearer'))
                                        <span class="error">{{ $errors->first('cost_bearer') }}</span>
                                    @endif
                                    <select class="form-control" name="cost_bearer" id="cost_bearer">
                                        <option @if($details->cost_bearer === "insurance") selected @endif value="insurance">Insurance</option>
                                        <option @if($details->cost_bearer === "samiti") selected @endif value="samiti">Samiti</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="type">Type<span class="astric">*</span></label>
                                    @if ($errors->has('type'))
                                        <span class="error">{{ $errors->first('type') }}</span>
                                    @endif
                                    <select class="form-control" name="type" id="type">
                                        <option value="">Select One</option>
                                        <option @if($details->type === "Type 1") selected @endif value="Type 1">Type 1</option>
                                        <option @if($details->type === "Type 2") selected @endif  value="Type 2">Type 2</option>
                                        <option @if($details->type === "Type 3") selected @endif  value="Type 3">Type 3</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="details">Details</label>
                                    <textarea class="form-control" name="details" rows="3" placeholder="Accident details">{{ $details->details }}</textarea>
                                </div>

                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary" name="submit-accident" value="Submit" />
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                </div>

                            </form>
                        </div></div></div></div>
        </section>
    </div>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2();

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
            //Datemask2 mm/dd/yyyy
            $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' });
            //Money Euro
            $('[data-mask]').inputmask();

            //Date range picker
            $('#reservation').daterangepicker();
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' });
            //Date range as a button
            $('#daterange-btn').daterangepicker(
                {
                    ranges   : {
                        'Today'       : [moment(), moment()],
                        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate  : moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
                }
            );

            //Date picker
            $('#datepicker').datepicker({
                autoclose: true
            });

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
            });
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass   : 'iradio_minimal-red'
            });
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass   : 'iradio_flat-green'
            });

            //Colorpicker
            $('.my-colorpicker1').colorpicker();
            //color picker with addon
            $('.my-colorpicker2').colorpicker();

            //Timepicker
            $('.timepicker').timepicker({
                showInputs: false
            });
        })
    </script>

    <script>
        $(function () {

            //Date picker
            $('#from').datepicker({
                autoclose: true
            });

            $('#to').datepicker({
                autoclose: true
            });

        });
    </script>


@endsection