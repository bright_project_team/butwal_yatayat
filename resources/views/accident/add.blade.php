@extends("../layout/master")
@include("layout.footer")
@include("layout.header")
@include("layout.meta")
@include("layout.side-bar")

@section("main-content")


    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <section class="content-header">
            <h1>Add new accident</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Accident</li>
                <li class="active">Add</li>
            </ol>

            @if(Session::has('message'))
                <p class="message"></p>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Success</h4>
                    {{ Session::get('message') }}
                </div>
            @endif
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <form action="{{ route('accident.store') }}" method="POST">

                        <div class="form-group" >
                            <label for="v_number">Vehicle Number<span class="astric">*</span></label>
                            @if ($errors->has('v_no'))
                                <span class="error">{{ $errors->first('v_no') }}</span>
                            @endif
                            <select class="form-control select2 select2-hidden-accessible" name="v_no" id="v_number"  style="width: 100%;">
                                style="width: 100%;">
                                <option value="" >Select Vehicle No</option>
                                @forelse($vehicles as $vehicle)
                                    <option value="{{$vehicle->v_no}}">{{$vehicle->v_no}}</option>
                                @empty
                                    <option>Not Found</option>
                                @endforelse
                            </select>
                        </div>

                        <div class="form-group" >
                            <label>Driver Name<span class="astric">*</span></label>
                            @if ($errors->has('d_name'))

                                <span class="error">{{ $errors->first('d_name') }}</span>
                            @endif
                            <select class="form-control select2" name="d_name" id="d_name">
                                style="width: 100%;">
                                <option value="" disabled selected>Driver Name</option>
                                @forelse($drivers as $driver)
                                    <option value="{{$driver->name}}">{{$driver->name}}</option>
                                @empty
                                    <option>Not Found</option>
                                @endforelse
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="location">Location<span class="astric">*</span></label>
                            @if ($errors->has('location'))
                                <span class="error">{{ $errors->first('location') }}</span>
                            @endif
                            <input id="location" class="form-control" type="text" name="location" placeholder="Eg. Butwal" />
                        </div>

                        <div class="form-group">
                            <label for="date">Date<span class="astric">*</span> </label>
                            @if ($errors->has('date'))
                                <span class="error">{{ $errors->first('date') }}</span>
                            @endif
                            <div class="input-group date">

                                <div class="input-group-addon" >
                                    <i class="fa fa-calendar"></i>
                                </div>

                                <input type="text" class="form-control pull-right" id="to" name="date">

                            </div>
                            <!-- /.input group -->
                        </div>
                        <div class="form-group">
                            <label for="cost_bearer">Cost Bearer<span class="astric">*</span></label>
                            @if ($errors->has('cost_bearer'))
                                <span class="error">{{ $errors->first('cost_bearer') }}</span>
                            @endif
                            <select class="form-control" name="cost_bearer" id="cost_bearer">
                                <option value="">Select One</option>
                                <option value="insurance">Insurance</option>
                                <option value="samiti">Samiti</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="type">Type<span class="astric">*</span></label>
                            @if ($errors->has('type'))
                                <span class="error">{{ $errors->first('type') }}</span>
                            @endif
                            <select class="form-control" name="type" id="type">
                                <option value="">Select One</option>
                                <option value="A">Type 1</option>
                                <option value="B">Type 2</option>
                                <option value="B">Type 3</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="details">Details</label>
                            <textarea class="form-control" name="details" rows="3" placeholder="Accident details"></textarea>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" name="submit-accident" value="Submit" />
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </div>

                    </form>
                </div>
            </div>
        </section>
    </div>

    <style>

    </style>
    <script>
        $(function () {
            $('.select2').select2();


            //Date picker
            $('#from').datepicker({
                autoclose: true
            });

            $('#to').datepicker({
                autoclose: true
            });

        });
    </script>
@endsection