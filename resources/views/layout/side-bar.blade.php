@section("side-bar-content")

    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{asset('images/user.svg')}}" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
              <li>
                  <a href="{{route('manage')}}">
                      <i class="fa fa-home"></i> Home
                  </a>

              </li>



                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-bus"></i> <span>Vehicles</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('vehicle.create')}}"><i class="fa fa-circle-o"></i> Add</a></li>
                        <li><a href="{{route('vehicle.index')}}"><i class="fa fa-circle-o"></i> View</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-users"></i> <span> Drivers</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">

                          <li><a href="{{route('driver.create')}}"><i class="fa fa-circle-o"></i> Add</a></li>
                        <li><a href="{{route('driver.index')}}"><i class="fa fa-circle-o"></i> View</a></li>


                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-suitcase"></i> <span>Owners</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{asset('owner/create')}}"><i class="fa fa-circle-o"></i> Add</a></li>
                        <li><a href="{{asset('owner')}}"><i class="fa fa-circle-o"></i> View</a></li>

                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-ship"></i> <span>Accidents</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>

                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{asset('accident/create')}}"><i class="fa fa-circle-o"></i> Add</a></li>
                        <li><a href="{{asset('accident')}}"><i class="fa fa-circle-o"></i> View</a></li>

                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-fax"></i> <span>Trip Fee</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                      <li><a href="{{asset('tripfee/create')}}"><i class="fa fa-circle-o"></i> Add</a></li>
                        <li><a href="{{asset('tripfee')}}"><i class="fa fa-circle-o"></i> View</a></li>

                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-ticket"></i> <span>Tickets</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                            <li><a href="{{route('ticket.create')}}"><i class="fa fa-circle-o"></i> Add</a></li>
                        <li><a href="{{asset('ticket')}}"><i class="fa fa-circle-o"></i> View</a></li>

                    </ul>
                </li>

                <li>
                    <a href="{{route('backup')}}">
                        <i class="fa fa-cogs"></i> Backup
                    </a>

                </li>
            </ul>
            </li>
            </ul>
            </ul>
            </li>
        </section>
        <!-- /.sidebar -->
    </aside>

    </a>
    <ul class="treeview-menu">
        <li><a href="{{asset('accident')}}"><i class="fa fa-circle-o"></i> View</a></li>
        <li><a href="{{asset('accident/create')}}"><i class="fa fa-circle-o"></i> Add</a></li>
    </ul>
    </li>
    </ul>
    </section>
    <!-- /.sidebar -->
    </aside>


@endsection
