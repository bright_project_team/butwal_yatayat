@section("footer-content")

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Develop by</b><a href="http://www.brightit.com.np"> Bright Office Systems Pvt. Ltd.</a>
    </div>
    <strong>Copyright &copy; 2017 <a href="#">Butwal Yatayat</a></strong> All rights
    reserved.
</footer>

@endsection
