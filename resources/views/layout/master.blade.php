<!DOCTYPE html>

<html lang="en-US">
<head>
    @yield("meta-content")
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    @yield("header-content")

    <!-- add left side bar section -->
    @yield("side-bar-content")

    <!-- add body section -->
    @yield("main-content")

    <!-- add footer section -->
    @yield("footer-content")

    <!-- add control-side-bar section -->
    @yield("control-side-bar-content")
</div>
</body>
</html>