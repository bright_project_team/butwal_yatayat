@section("header-content")


    <header class="main-header">
        <!-- Logo -->
        <a href="{{route('manage')}}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>B</b>Y</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Butwal</b>Yatayat</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">

            <!-- Sidebar toggle button-->
            <a class="sidebar-toggle" role="button" href="#" data-toggle="push-menu">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->
                    <!-- Notifications: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{ asset('images/user.svg') }}" class="user-image" alt="User Image">
                            <span class="hidden-xs">{{ Auth::user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="{{ asset('admin/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">

                                <p>
                                    {{ Auth::user()->name }}
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                  @php $type= Auth::user()->user_type; @endphp
                                  @if($type=='admin')
                                    <a href="{{ route('register') }}" class="btn btn-default btn-flat">Register</a>
                                    @endif
                                </div>
                                <div class="pull-right">
                                    <a href="{{route('logout')}}" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown notifications-menu">
                        <a class="dropdown-toggle" aria-expanded="false" href="#" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <span class="label label-danger">{{ count($expired) }}</span>

                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">Insurance renewal notice</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu">
                                    @foreach($expired as $expired_vehicle)
                                        <?php $date1 = date('y-m-d');
                                        $date2 = $expired_vehicle->date_to;

                                        $diff = strtotime($date2) - strtotime($date1);
                                        $diff = $diff/86400;
                                        ?>

                                        <li class="margin">
                                            <span style="text-transform: uppercase;">{{ $expired_vehicle->v_no }}</span>
                                            @if($diff < 0)
                                                <span style="color:maroon;">{{ abs($diff) }} days has passed.</span>
                                            @else
                                                <span style="color:orange;">{{ abs($diff) }} days remaining.</span>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <!-- Tasks: style can be found in dropdown.less -->
                </ul>
            </div>

        </nav>

    </header>

@endsection
