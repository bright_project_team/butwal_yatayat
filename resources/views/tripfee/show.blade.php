@extends("../layout/master")
@include("layout.footer")
@include("layout.header")
@include("layout.meta")
@include("layout.side-bar")

@section("main-content")


    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <section class="content-header">
            <h1>Tripfees</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Tripfee</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body">
                            @if(Session::has('message'))
                                <p class="message"></p>
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h4><i class="icon fa fa-check"></i> Success</h4>
                                    {{ Session::get('message') }}
                                </div>
                            @endif
                            <table id="example1" class="table table-bordered table-striped ">
                                <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Vehicle No</th>

                                    <th>Trip Type</th>
                                    <th>Amount First</th>
                                    <th>Amount Second</th>
                                    <th>Total</th>
                                    <th>From Date</th>
                                    <th>To Date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($tripfee as $data)

                                    <tr>
                                        <a href="#"><td>{{$loop->index+1}}</td></a>
                                        <td>{{$data->v_no}}</td>

                                        <td> {{$data->t_type  }}</td>
                                        <td>{{$data->amount_first  }}</td>
                                        <td>{{$data->amount_second  }}</td>
                                        <td><?php echo (int) $data->amount_first + (int) $data->amount_second ?></td>
                                        <td>{{$data->start_date  }}</td>
                                        <td>{{$data->end_date  }}</td>
                                        <td>

                                          @php $type= Auth::user()->user_type; @endphp
                                          @if($type=='admin')


                                            <form id="delete-item{{$data->id}}"  action="{{route('tripfee.destroy',$data->id)}}" style="Display:none"  method="post">

                                                {{csrf_field()}}
                                                {{method_field('DELETE')}}

                                            </form>

                                            <a href="" onclick="
                                                    if (confirm('Are you sure, You want to delete this ?'))
                                                    {
                                                    event.preventDefault();
                                                    document.getElementById('delete-item{{$data->id}}').submit();
                                                    }
                                                    else{
                                                    event.preventDefault();
                                                    }

                                                    ">

                                                <i class="fa fa-fw fa-trash"></i></a>

                                                ||
                                               @endif
                                                <a href="{{route('tripfee.show',$data->id)}}" target="_blank"><i class="fa fa-print" aria-hidden="true"></i></a>

                                        </td>
                                    </tr>
                                @endforeach

                                </tfoot>
                            </table>




                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
        </section>

    </div>

    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>


    <style>
        .expired{
            background: #f4ab90;

        }
    </style>
@endsection
