@extends("../layout/master")
@include("layout.footer")
@include("layout.header")
@include("layout.meta")
@include("layout.side-bar")

@section("main-content")


    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <section class="content-header">
            <h1>Add new tripfee</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Trip Fee</li>
                <li class="active">Add</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <form action="{{ route('tripfee.store') }}" method="POST">
                        <div class="form-group" >
                            <label>Vehicle Number<span class="astric">*</span></label>
                            @if ($errors->has('v_number'))

                                <span class="error">{{ $errors->first('v_number') }}</span>
                            @endif
                            <select class="form-control select2 select2-hidden-accessible" name="v_number" id="v_number" style="width: 100%;" >
                                style="width: 100%;">
                                <option value="" >Select Vehicle No</option>
                                @forelse($vehicle as $data)
                                    <option value="{{$data->v_no}}">{{$data->v_no}}</option>
                                @empty
                                    <option>Not Found</option>
                                @endforelse
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Trip Type<span class="astric">*</span></label>
                            @if ($errors->has('t_type'))
                                <span class="error">{{ $errors->first('t_type') }}</span>
                            @endif
                            <select class="form-control" name="t_type" id="trip-type">
                                <option value="">Select One</option>
                                <option value="daily">Daily</option>
                                <option value="semi_monthly">Semi Monthly</option>
                                <option value="monthly">Monthly</option>

                            </select>
                        </div>
                        <div class="form-group">
                            <label for="amount-first">Amount First<span class="astric">*</span></label>
                            @if ($errors->has('a_first'))
                                <span class="error">{{ $errors->first('a_first') }}</span>
                            @endif
                            <input id="amount-first" class="form-control" type="text" name="a_first" placeholder="Eg. Rs 2000" />
                        </div>
                        <div class="form-group">
                            <label for="amount-second">Amount Second<span class="astric">*</span></label>
                            @if ($errors->has('a_second'))
                                <span class="error">{{ $errors->first('a_second') }}</span>
                            @endif
                            <input id="amount-second" class="form-control" type="text" name="a_second" placeholder="Eg. Rs 1000" />
                        </div>
                        <div class="form-horizontal row">
                            <label class="col-lg-12">Trip Duration</label>
                            <div class="form-group col-md-6">
                                <label for="vehicle-route-from" class="col-sm-2 control-label">From<span class="astric">*</span> </label>
                                @if($errors->has('date_from'))
                                    <span class="error">  {{  $errors->first('date_from')}}</span>
                                @endif
                                <div class="input-group date">

                                    <div class="input-group-addon" >
                                        <i class="fa fa-calendar"></i>
                                    </div>

                                    <input type="text" class="form-control pull-right" id="from" name="date_from">

                                </div>
                                <!-- /.input group -->
                            </div>
                            <div class="form-group col-md-6">
                                <label for="vehicle-route-from" class="col-sm-2 control-label">To<span class="astric">*</span> </label>
                                @if($errors->has('date_to'))
                                    <span class="error">  {{  $errors->first('date_to')}}</span>
                                @endif
                                <div class="input-group date">

                                    <div class="input-group-addon" >
                                        <i class="fa fa-calendar"></i>
                                    </div>

                                    <input type="text" class="form-control pull-right" id="to" name="date_to">

                                </div>
                                <!-- /.input group -->
                            </div>

                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" name="submit-vehicle" value="Submit" />
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>

    <style>

    </style>
    <script>
        $(function () {


             $('.select2').select2();

            $("#from").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayBtn: "linked",

                todayHighlight: true,
            }).on('changeDate', function (selected) {
                var startDate = new Date(selected.date.valueOf());
                $('#to').datepicker('setStartDate', startDate);
            }).on('clearDate', function (selected) {
                $('#to').datepicker('setStartDate', null);
            });

            $("#to").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayBtn: "linked",

                todayHighlight: true,
            }).on('changeDate', function (selected) {
                var endDate = new Date(selected.date.valueOf());
                $('#from').datepicker('setEndDate', endDate);
            }).on('clearDate', function (selected) {
                $('#from').datepicker('setEndDate', null);

            });

        });
    </script>
@endsection