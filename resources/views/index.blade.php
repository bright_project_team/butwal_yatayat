@extends("layout.master")
@include("layout.footer")
@include("layout.header")
@include("layout.meta")
@include("layout.side-bar")


@section("main-content")
@if(isset($asdf)) {{ $asdf }} @endif


    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <section class="content-header">
            <h1>Butwal Yatayat </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>

        <section class="content">
            @if(isset($msg))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Success</h4>
                {{ $msg }}
            </div>
            @endif

            <div class="row">
                <a  href="{{route('vehicle.index')}}">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-aqua"><i class="fa fa-bus"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text"> total Vehicles</span>
                                <span class="info-box-number">{{count($vehicles)}}</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>

                </a>
                <!-- /.col -->
                <a  href="{{route('owner.index')}}">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-green"><i class="fa fa-suitcase"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Total Owners</span>
                                <span class="info-box-number">{{count($owners)}}</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                </a>
                <!-- /.col -->
                <a  href="{{route('driver.index')}}">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-yellow"><i class="fa fa-user"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Total drivers</span>
                                <span class="info-box-number">{{count($drivers)}}</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                </a>
                <!-- /.col -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-red"><i class="fa fa-star-o"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Insurance Expired Vehicles</span>
                            <span class="info-box-number">{{count($expired)}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>





                <!-- /.col -->
            </div>
            @if(Session::has('message'))
                <div class="alert alert-danger alert-dismissable" style="margin-bottom: 0px !important;">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <strong>Error!</strong> {{ Session::get('message') }}
                </div>
            @endif
            <div class="box adjusted-height">
                <div class="box-header with-border">

                    <h3 class="box-title">Report</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <form action="{{ route('report') }}" method="POST">
                        {{ csrf_field()}}
                        <div class="form-horizontal row">
                            <label class="col-lg-12">Trip Fee</label>
                            <div class="form-group col-md-6">
                                <label for="date_from" class="col-sm-2 control-label">From<span class="astric">*</span> </label>
                                @if($errors->has('date_from'))
                                    <span class="error">  {{  $errors->first('date_from')}}</span>
                                @endif
                                <div class="input-group date">

                                    <div class="input-group-addon" >
                                        <i class="fa fa-calendar"></i>
                                    </div>

                                    <input type="text" class="form-control pull-right" id="date_from" name="date_from">

                                </div>
                                <!-- /.input group -->
                            </div>
                            <div class="form-group col-md-6">
                                <label for="date_to" class="col-sm-2 control-label">To<span class="astric">*</span> </label>
                                @if($errors->has('date_to'))
                                    <span class="error">  {{  $errors->first('date_to')}}</span>
                                @endif
                                <div class="input-group date">

                                    <div class="input-group-addon" >
                                        <i class="fa fa-calendar"></i>
                                    </div>

                                    <input type="text" class="form-control pull-right" id="date_to" name="date_to">

                                </div>
                                <!-- /.input group -->
                            </div>

                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" name="submit-vehicle" value="Submit" />

                        </div>
                    </form>
                </div>
                <!-- /.box-body -->

                <!-- /.box-footer-->
            </div>
            <div class="box adjusted-height">
                <div class="box-header with-border">
                    <h3 class="box-title">Report</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <form action="{{ route('reports') }}" method="POST">
                        {{ csrf_field()}}
                        <div class="form-horizontal row">
                            <label class="col-lg-12">Insurance</label>
                            <div class="form-group col-md-6">
                                <label for="insurance_from" class="col-sm-2 control-label">From<span class="astric">*</span> </label>
                                @if($errors->has('insurance_from'))
                                    <span class="error">  {{  $errors->first('insurance_from')}}</span>
                                @endif
                                <div class="input-group date">

                                    <div class="input-group-addon" >
                                        <i class="fa fa-calendar"></i>
                                    </div>

                                    <input type="text" class="form-control pull-right" id="insurance_from" name="insurance_from">

                                </div>
                                <!-- /.input group -->
                            </div>
                            <div class="form-group col-md-6">
                                <label for="insurance_to" class="col-sm-2 control-label">To<span class="astric">*</span> </label>
                                @if($errors->has('insurance_to'))
                                    <span class="error">  {{  $errors->first('insurance_to')}}</span>
                                @endif
                                <div class="input-group date">

                                    <div class="input-group-addon" >
                                        <i class="fa fa-calendar"></i>
                                    </div>

                                    <input type="text" class="form-control pull-right" id="insurance_to" name="insurance_to">

                                </div>
                                <!-- /.input group -->
                            </div>

                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" name="submit-vehicle" value="Submit" />

                        </div>
                    </form>
                </div>
                <!-- /.box-body -->

                <!-- /.box-footer-->
            </div>
            <div class="box adjusted-height">
                <div class="box-header with-border">
                    <h3 class="box-title">Report</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <form action="{{ route('reportss') }}" method="POST">
                        {{ csrf_field()}}
                        <div class="form-horizontal row">
                            <label class="col-lg-12">Ticket</label>
                            <div class="form-group col-md-6">
                                <label for="ticket_from" class="col-sm-2 control-label">From<span class="astric">*</span> </label>
                                @if($errors->has('ticket_from'))
                                    <span class="error">  {{  $errors->first('ticket_from')}}</span>
                                @endif
                                <div class="input-group date">

                                    <div class="input-group-addon" >
                                        <i class="fa fa-calendar"></i>
                                    </div>

                                    <input type="text" class="form-control pull-right" id="ticket_from" name="ticket_from">

                                </div>
                                <!-- /.input group -->
                            </div>
                            <div class="form-group col-md-6">
                                <label for="ticket_to" class="col-sm-2 control-label">To<span class="astric">*</span> </label>
                                @if($errors->has('ticket_to'))
                                    <span class="error">  {{  $errors->first('ticket_to')}}</span>
                                @endif
                                <div class="input-group date">

                                    <div class="input-group-addon" >
                                        <i class="fa fa-calendar"></i>
                                    </div>

                                    <input type="text" class="form-control pull-right" id="ticket_to" name="ticket_to">

                                </div>
                                <!-- /.input group -->
                            </div>

                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" name="submit-vehicle" value="Submit" />

                        </div>
                    </form>
                </div>
                <!-- /.box-body -->

                <!-- /.box-footer-->
            </div>
        </section>

    </div>
    <script>
        $(function () {


//datepicker for tripfee report

            $("#date_from").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayBtn: "linked",

                todayHighlight: true,
            }).on('changeDate', function (selected) {
                var startDate = new Date(selected.date.valueOf());
                $('#date_to').datepicker('setStartDate', startDate);
            }).on('clearDate', function (selected) {
                $('#date_to').datepicker('setStartDate', null);
            });

            $("#date_to").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayBtn: "linked",

                todayHighlight: true,
            }).on('changeDate', function (selected) {
                var endDate = new Date(selected.date.valueOf());
                $('#date_from').datepicker('setEndDate', endDate);
            }).on('clearDate', function (selected) {
                $('#date_from').datepicker('setEndDate', null);

            });


            //datepicker for insurance report
            $("#insurance_from").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayBtn: "linked",

                todayHighlight: true,
            }).on('changeDate', function (selected) {
                var startDate = new Date(selected.date.valueOf());
                $('#insurance_to').datepicker('setStartDate', startDate);
            }).on('clearDate', function (selected) {
                $('#insurance_to').datepicker('setStartDate', null);
            });

            $("#insurance_to").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayBtn: "linked",

                todayHighlight: true,
            }).on('changeDate', function (selected) {
                var endDate = new Date(selected.date.valueOf());
                $('#insurance_from').datepicker('setEndDate', endDate);
            }).on('clearDate', function (selected) {
                $('#insurance_from').datepicker('setEndDate', null);

            });

//datepicker for ticket report

            $("#ticket_from").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayBtn: "linked",

                todayHighlight: true,
            }).on('changeDate', function (selected) {
                var startDate = new Date(selected.date.valueOf());
                $('#ticket_to').datepicker('setStartDate', startDate);
            }).on('clearDate', function (selected) {
                $('#ticket_to').datepicker('setStartDate', null);
            });

            $("#ticket_to").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayBtn: "linked",

                todayHighlight: true,
            }).on('changeDate', function (selected) {
                var endDate = new Date(selected.date.valueOf());
                $('#ticket_from').datepicker('setEndDate', endDate);
            }).on('clearDate', function (selected) {
                $('#ticket_from').datepicker('setEndDate', null);

            });


        });



    </script>
    <style>
        .astric,.error{
            color:red;
        }



    </style>
@endsection
