<?php

use App\owner;

?>

@extends("../layout/master")
@include("layout.footer")
@include("layout.header")
@include("layout.meta")
@include("layout.side-bar")

@section("main-content")


    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <section class="content-header">
            <h1>Report Between <span style="font-size: 16px;color: blue;"> {{$i_from}}</span> and <span style="font-size: 16px;color: blue;"> {{$i_to}}</span> </h1>

            <a class="export" href="{{route('exportInsurance')}}"><button type="submit" class="btn btn-success">Export in Excel</button></a>




            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>

            <div class="box">

                <div class="box-body">

                    <table id="example1" class="table table-bordered table-striped ">
                        <thead>
                        <tr>
                            <th>SN</th>
                            <th>Vehicle No</th>
                            <th>Owner Name</th>
                            <th>Owner Contact</th>

                            <th>Expire Date</th>


                        </tr>
                        </thead>
                        <tbody>

                        @foreach($insurance as $data)


                            <?php
                            $currentdate = date('Y-m-d'); $diff = strtotime($data->date_to) - strtotime($currentdate);
                            $diffs = $diff/86400;

                            if($diffs<=0){
                                $espired = TRUE;

                            }
                            else{
                                $expired = FALSE;
                            }
                            ?>

                            <tr <?php if($expired){echo 'style="background: #f4ab90;"';}?> >
                                <a href="#"><td>{{$loop->index+1}}</td></a>
                                <td>{{$data->v_no}}</td>

                                <?php
                                $owners = owner::all();
                                foreach($owners as $owner){
                                    $v_no_array = unserialize($owner->v_no);
                                    foreach($v_no_array as $v_no){
                                        if($v_no == $data->v_no){
                                            $owner_name = $owner->name;
                                            $owner_contact =$owner->contact_no;
                                            break;
                                        }
                                    }
                                }
                                ?>

                                <td>{{$owner_name}}</td>
                                <td>{{$owner_contact}}</td>

                                <td>{{$data->date_to  }} (<?php  if($diffs>=0){echo $diffs.' days left';}else {echo 'expired';}  ?>)</td>

                            </tr>
                        @endforeach
                        </tbody>

                    </table>




                </div>
                <!-- /.box-body -->
            </div>






        </section>

    </div>

    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>


    <style>
        .expired{
            background: #f4ab90;

        }
        section a.export{
            float: right;
            background: transparent;
            margin-top: 0;
            margin-bottom: 0px;
            font-size: 12px;
            padding: 7px 5px;
            position: absolute;
            top: -3px;
            right: 200px;
        }
    </style>
@endsection
