<?php

use App\owner;

?>

@extends("../layout/master")
@include("layout.footer")
@include("layout.header")
@include("layout.meta")
@include("layout.side-bar")

@section("main-content")


    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <section class="content-header">
            <h1>Report of Tripfee Between <span style="font-size: 16px;color: blue;"> {{$from}}</span> and <span style="font-size: 16px;color: blue;"> {{$to}}</span> </h1>


            <a class="export" href="{{route('export')}}"><button type="submit" class="btn btn-success">Export in Excel</button></a>




            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>







        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        @if(Session::has('flash_message'))
                            <div  class="alert alert-success">
                                {{Session::get('flash_message')}}
                            </div>

                        @endif
                        <div class="box-body">

                            <table id="example1" class="table table-bordered table-striped ">
                                <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Vehicle No</th>
                                    <th>Owner Name</th>

                                    <th>Trip Type</th>
                                    <th>Amount First</th>
                                    <th>Amount Second</th>
                                    <th>Total</th>
                                    <th>From Date</th>
                                    <th>To Date</th>

                                </tr>
                                </thead>
                                <tbody>

                                @foreach($tripfee as $data)


                                    <tr>
                                        <a href="#"><td>{{$loop->index+1}}</td></a>
                                        <td>{{$data->v_no}}</td>

                                        <?php
                                        $owners = owner::all();
                                        foreach($owners as $owner){
                                            $v_no_array = unserialize($owner->v_no);
                                            foreach($v_no_array as $v_no){
                                                if($v_no == $data->v_no){
                                                    $owner_name = $owner->name;
                                                    break;
                                                }
                                            }
                                        }
                                        ?>

                                        <td>{{$owner_name}}</td>

                                        <td> {{$data->t_type  }}</td>
                                        <td>{{$data->amount_first  }}</td>
                                        <td>{{$data->amount_second  }}</td>
                                        <td>{{$data->amount_first + $data->amount_second}}</td>
                                        <td>{{$data->start_date  }}</td>
                                        <td>{{$data->end_date  }}</td>

                                    </tr>
                                @endforeach
                                <?php if(count($tripfee)>0){?>
                              <tfoot>
                              <td></td>   <td></td>  <td></td>  <td></td>  <td></td>  <td></td>  <td>@foreach($total as $data)Total: {{$data->count}}@endforeach</td><td></td><td></td>
                                </tfoot>
                                <?php }?>
                            </table>




                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
        </section>

    </div>

    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>


    <style>
        .expired{
            background: #f4ab90;

        }
        section a.export{
            float: right;
            background: transparent;
            margin-top: 0;
            margin-bottom: 0px;
            font-size: 12px;
            padding: 7px 5px;
            position: absolute;
            top: -3px;
            right: 200px;
        }
    </style>
@endsection
