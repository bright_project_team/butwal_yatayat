
@extends("../layout/master")
@include("layout.footer")
@include("layout.header")
@include("layout.meta")
@include("layout.side-bar")

@section("main-content")


    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <section class="content-header">
            <h1>Report of Ticket Between <span style="font-size: 16px;color: blue;"> {{$t_from}}</span> and <span style="font-size: 16px;color: blue;"> {{$t_to}}</span> </h1>

            <a class="export" href="{{route('ticketReportPrint')}}" target="_blank"><button type="submit" class="btn btn-success">Print</button></a>




            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>

            <div class="box">

                <div class="box-body">

                    <table id="example1" class="table table-bordered table-striped ">
                        <thead>
                        <tr>
                            <th>SN</th>
                            <th>Vehicle No</th>
                            <th>Name</th>
                            <th>Particular</th>
                            <th>Amount</th>
                            <th>Date</th>


                        </tr>
                        </thead>
                        <tbody>

                        @foreach($ticket as $data)

                            <tr>
                               <td>{{$loop->index+1}}</td>
                                <td>@php if(!empty($data->v_no)) {echo $data->v_no;} else {echo '-';} @endphp</td>

                                <td>{{$data->name}}</td>
                                <td>{{$data->particular}}</td>
                                <td>{{$data->amount}}</td>
                                <td>{{date('Y-m-d',strtotime($data->created_at))  }} </td>

                            </tr>
                        @endforeach
                        </tbody>
                        <?php if(count($ticket)>0){?>
                        <tfoot>
                        <td></td><td></td><td></td><td></td>
                          <td>
                            @foreach($sum as $data)
                           Total: {{$data->count}}
                                @endforeach
                        </td>
                        <td></td>
                        </tfoot>
                       <?php }?>

                    </table>




                </div>
                <!-- /.box-body -->
            </div>






        </section>

    </div>

    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>


    <style>

        section a.export{
            float: right;
            background: transparent;
            margin-top: 0;
            margin-bottom: 0px;
            font-size: 12px;
            padding: 7px 5px;
            position: absolute;
            top: -3px;
            right: 200px;
        }
    </style>
@endsection
