@extends("../layout/master")
@include("layout.footer")
@include("layout.header")
@include("layout.meta")
@include("layout.side-bar")

@section("main-content")


    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <section class="content-header">
            <h1>Add new vehicle</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Vehicle</li>
                <li class="active">Add</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <form action="{{route('vehicle.store')}}" method="post" name="form">
                        {{ csrf_field()}}

                        <div class="form-group">


                            <label for="vehicle-number">Number<span class="astric">*</span>  </label>@if($errors->has('v_no'))
                                <span class="error">  {{  $errors->first('v_no')}}</span>
                            @endif
                            <input id="vehicle-number" class="form-control" type="text" name="v_no" placeholder="Eg. Lu 2 Pa 2345" />
                        </div>
                        <div class="form-group">
                            <label for="vehicle-company">Company<span class="astric">*</span> </label>@if($errors->has('v_company'))
                                <span class="error">  {{  $errors->first('v_company')}}</span>
                            @endif
                            <input id="vehicle-company" class="form-control" type="text" name="v_company" placeholder="Eg. Tata, Ford, Suzuki" />
                        </div>
                        <div class="form-group">
                            <label for="vehicle-model">Model<span class="astric">*</span> </label>
                            @if($errors->has('v_company'))
                                <span class="error">  {{  $errors->first('v_model')}}</span>
                            @endif
                            <input id="vehicle-model" class="form-control" type="text" name="v_model" placeholder="Eg. Hyundai Elite i20" />
                        </div>
                        <div class="form-group">
                            <label for="vehicle-model">Chassis Number<span class="astric">*</span> </label>
                            @if($errors->has('chassis_no'))
                                <span class="error">  {{  $errors->first('chassis_no')}}</span>
                            @endif
                            <input id="chassis_no" class="form-control" type="text" name="chassis_no" placeholder="Eg. SV30-0169266 " />
                        </div>
                        <div class="form-group">
                            <label for="vehicle-type">Type<span class="astric">*</span> </label>
                            @if($errors->has('v_company'))
                                <span class="error">  {{  $errors->first('v_type')}}</span>
                            @endif
                            <input id="vehicle-type" class="form-control" type="text" name="v_type" placeholder="Eg. Jeep, Mini-Bus, Micro-Bus" />
                        </div>

                        <div class="form-horizontal row">
                            <label class="col-lg-12">Insurance</label>
                            <div class="form-group col-md-6">
                                <label for="vehicle-route-from" class="col-sm-2 control-label">From<span class="astrict">*</span> </label>
                                @if($errors->has('date_from'))
                                    <span class="error">  {{  $errors->first('date_from')}}</span>
                                @endif
                                <div class="input-group date">

                                    <div class="input-group-addon" >
                                        <i class="fa fa-calendar"></i>
                                    </div>

                                    <input type="text" class="form-control pull-right" id="from" name="date_from">

                                </div>
                                <!-- /.input group -->
                            </div>
                            <div class="form-group col-md-6">
                                <label for="vehicle-route-from" class="col-sm-2 control-label">To<span class="astrict">*</span> </label>
                                @if($errors->has('date_to'))
                                    <span class="error">  {{  $errors->first('date_to')}}</span>
                                @endif
                                <div class="input-group date">

                                    <div class="input-group-addon" >
                                        <i class="fa fa-calendar"></i>
                                    </div>

                                    <input type="text" class="form-control pull-right" id="to" name="date_to">

                                </div>
                                <!-- /.input group -->
                            </div>

                        </div>

                        <div class="form-horizontal">
                            <label>Route</label>
                            <div class="form-group">
                                <label for="vehicle-route-from" class="col-sm-1 control-label">From<span class="astric">*</span> </label>
                                @if($errors->has('v_company'))
                                    <span class="error">  {{  $errors->first('from')}}</span>
                                @endif
                                <div class="col-sm-11">
                                    <input type="text" class="form-control" id="vehicle-route-from" placeholder="Eg. Butwal" name="from">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="vehicle-route-to" class="col-sm-1 control-label">To<span class="astric">*</span> </label>
                                @if($errors->has('v_company'))
                                    <span class="error">  {{  $errors->first('to')}}</span>
                                @endif
                                <div class="col-sm-11">
                                    <input type="text" class="form-control" id="vehicle-route-to" placeholder="Eg. Kathmandu" name="to">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" name="submit-vehicle" value="Submit" />

                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
    <script>
        $(function () {




            $("#from").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayBtn: "linked",
                endDate:'today',
                todayHighlight: true,
            }).on('changeDate', function (selected) {
                var startDate = new Date(selected.date.valueOf());
                $('#to').datepicker('setStartDate', startDate);
            }).on('clearDate', function (selected) {
                $('#to').datepicker('setStartDate', null);
            });

            $("#to").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayBtn: "linked",

                todayHighlight: true,
            }).on('changeDate', function (selected) {
                var endDate = new Date(selected.date.valueOf());
                $('#from').datepicker('setEndDate', endDate);
            }).on('clearDate', function (selected) {
                $('#from').datepicker('setEndDate', null);

            });

        });
    </script>

@endsection
