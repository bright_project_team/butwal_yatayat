@extends("../layout/master")
@include("layout.footer")
@include("layout.header")
@include("layout.meta")
@include("layout.side-bar")

@section("main-content")


    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <section class="content-header">
            <h1>Vehicles</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Vehicle</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body">
                            @if(Session::has('message'))
                                <p class="message"></p>
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h4><i class="icon fa fa-check"></i> Success</h4>
                                    {{ Session::get('message') }}
                                </div>
                            @endif
                            <table id="example1" class="table table-bordered table-striped ">
                                <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Vehicle No</th>
                                    <th>Company Name</th>
                                    <th>Vehicle Model</th>
                                    <th>chassis Number</th>
                                    <th>vehicle Type</th>
                                    <th>Route From</th>
                                    <th>Route To</th>
                                    <th>Registered Date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($vehicles as $vehicle)

                                    <?php
                                    $currnet_data = date('Y-m-d');
                                    $last_date = $vehicle->date_to;

                                    $dateTimestamp1 = strtotime($currnet_data);
                                    $dateTimestamp2 = strtotime($last_date);
                                    $diff= $dateTimestamp2-$dateTimestamp1;
                                    $days = floor($diff / (60 * 60 * 24));

                                    //echo $diff;
                                    if ($days<=0)
                                    {
                                        $expired  = TRUE;

                                    }

                                    else {
                                        $expired  = FALSE;


                                    }


                                    ?>




                                    <tr <?php if($expired){echo 'style="background: #f4ab90;"';} ?> >



                                        <a href="#"><td>{{$loop->index+1}}</td></a>
                                        <td><a href="{{ route('vehicle.show',$vehicle->v_no) }}">{{$vehicle->v_no}}</a></td>
                                        <td>{{$vehicle->company_name }}
                                        </td>
                                        <td>{{$vehicle->model }}</td>
                                          <td>{{$vehicle->chassis_no }}</td>
                                        <td> {{$vehicle->v_type  }}</td>
                                        <td>{{$vehicle->route_from  }}</td>
                                        <td>{{$vehicle->route_to  }}</td>
                                        <td>{{$vehicle->created_at  }}</td>
                                        <td>
                                          @php $type= Auth::user()->user_type; @endphp
                                          @if($type=='admin')
                                            <a href="{{route('vehicle.edit',$vehicle->id)}}">   <i class="fa fa-fw fa-edit"></i></a>||
                                            <form id="delete-item{{$vehicle->id}}"  action="{{route('vehicle.destroy',$vehicle->id)}}" style="Display:none"  method="post">
                                                {{csrf_field()}}
                                                {{method_field('DELETE')}}

                                            </form>
                                            <a href="" onclick="
                                                    if (confirm('Are you sure, You want to delete this ?'))
                                                    {
                                                    event.preventDefault();
                                                    document.getElementById('delete-item{{$vehicle->id}}').submit();
                                                    }
                                                    else{
                                                    event.preventDefault();
                                                    }

                                                    ">

                                                <i class="fa fa-fw fa-trash"></i></a>
                                                @endif
                                        </td>
                                    </tr>
                                @endforeach()

                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>


    <style>
        .expired{
            background: #f4ab90;

        }
    </style>
@endsection
