@extends("../layout/master")
@include("layout.footer")
@include("layout.header")
@include("layout.meta")
@include("layout.side-bar")

@section("main-content")


    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <section class="content-header">
            <h1>Vehicle Details</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Vehicle</li>
                <li class="active">Detail</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        @if(Session::has('flash_message'))
                            <div  class="alert alert-success">
                                {{Session::get('flash_message')}}
                            </div>

                        @endif
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- Custom Tabs -->
                                    <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#vehicle" data-toggle="tab">Vehicle</a></li>
                                            <li><a href="#owner" data-toggle="tab">Owner</a></li>
                                            <li><a href="#driver" data-toggle="tab">Driver</a></li>
                                            <li><a href="#tripfees" data-toggle="tab">Tripfees</a></li>
                                            <li><a href="#tickets" data-toggle="tab">Tickets</a></li>
                                            <li><a href="#accidents" data-toggle="tab">Accidents</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="vehicle">
                                                <table id="example1" class="table table-bordered table-striped">
                                                    <h2>Vehicle</h2>
                                                    <tr>
                                                        <th>Vehicle Insurance  status</th>
                                                        <?php
                                                        $currnet_data = date('Y-m-d');
                                                        $last_date = $vehicle->date_to;

                                                        $dateTimestamp1 = strtotime($currnet_data);
                                                        $dateTimestamp2 = strtotime($last_date);
                                                        $diff= $dateTimestamp2-$dateTimestamp1;
                                                        $days = floor($diff / (60 * 60 * 24));


                                                        if ($days<=0)
                                                        {?>
                                                        <td> <button style="width:20%;" class="btn btn-block btn-danger btn-xs " type="button">Expired</button></td>

                                                        <?php }

                                                        else { ?>

                                                        <td> <button style="width:20%;" class="btn btn-block btn-success btn-xs " type="button">Active</button></td>

                                                        <?php  } ?>


                                                    </tr>
                                                    <tr>
                                                        <th>Vehicle No.</th>
                                                        <td>{{ $vehicle->v_no }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Company</th>
                                                        <td>{{ $vehicle->company_name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Model</th>
                                                        <td>{{ $vehicle->model }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Type</th>
                                                        <td>{{ $vehicle->v_type }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Route</th>
                                                        <td>{{ $vehicle->route_from }} to {{ $vehicle->route_to }}</td>
                                                    </tr>
                                                    <?php
                                                    if($vehicle->date_to < date('y-m-d')){
                                                        $expired = TRUE;
                                                    }else{
                                                        $expred = FALSE;
                                                    }
                                                    ?>
                                                    <tr <?php if($expired){echo "class='expired'";} ?>>
                                                        <th>Insurance (last date)</th>

                                                        @if($diff < 0)
                                                            <td>{{ $vehicle->date_to }}  (<span class="remaining-days  ">{{ abs($days) }} days has passed.</span>)</td>
                                                        @else
                                                            <td>
                                                                {{ $vehicle->date_to }}
                                                                (<span class="remaining-days">{{ abs($days) }} days remaining.</span>)
                                                            </td>
                                                            @endif
                                                            </span></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Vehicle Registration Date</th>
                                                        <td>{{ $vehicle->created_at }}</td>
                                                    </tr>
                                                </table>
                                            </div>

                                            @if(isset($owner_detail))
                                                <div class="tab-pane" id="owner">
                                                    <table id="example1" class="table table-bordered table-striped">
                                                        <h2>Owner</h2>
                                                        <tr>
                                                            <th>Name</th>
                                                            <td>{{ $owner_detail['name'] }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Address</th>
                                                            <td>{{ $owner_detail['address'] }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Contact</th>
                                                            <td>{{ $owner_detail['contact'] }}</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            @else
                                                <div class="tab-pane" id="owner">
                                                    <div  class="alert alert-success">
                                                        No owner.
                                                    </div>
                                                </div>
                                            @endif

                                            @if(isset($driver_detail))
                                                <div class="tab-pane" id="driver">
                                                <table id="example1" class="table table-bordered table-striped">
                                                    <h2>Driver</h2>
                                                    <tr>
                                                        <th>Card No.</th>
                                                        <th>Name</th>
                                                        <th>Address</th>
                                                        <th>Contact</th>
                                                        <th>License No.</th>
                                                        <th>Age</th>
                                                        <th>Gender</th>
                                                        <th>Blood Group</th>
                                                        <th>Experience</th>
                                                        <th>Type</th>
                                                        <th>Card Expiry Date</th>
                                                    </tr>
                                                    @foreach($driver_detail as $driver)
                                                        <tr>
                                                        <td>{{ $driver['card_no'] }}</td>
                                                        <td>{{ $driver['name'] }}</td>
                                                        <td>{{ $driver['address'] }}</td>
                                                        <td>{{ $driver['contact'] }}</td>
                                                        <td>{{ $driver['license_no'] }}</td>
                                                        <td>{{ $driver['age'] }}</td>
                                                        <td>{{ $driver['gender'] }}</td>
                                                        <td>{{ $driver['blood_group'] }}</td>
                                                        <td>{{ $driver['experience'] }}</td>
                                                        <td>{{ $driver['type'] }}</td>
                                                        <td>{{ $driver['card_expiry_date'] }}</td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </div>
                                            @else
                                                <div class="tab-pane" id="driver">
                                                    <div  class="alert alert-success">
                                                        No driver.
                                                    </div>
                                                </div>
                                            @endif

                                            @if(count($tripfees) > 0)
                                                <div class="tab-pane" id="tripfees">
                                                    <table id="example1" class="table table-bordered table-striped">
                                                        <tr>
                                                            <th>Register Date</th>
                                                            <th>Type</th>
                                                            <th>Start Date</th>
                                                            <th>End Date</th>
                                                            <th>Amount First</th>
                                                            <th>Amount Second</th>
                                                            <th>Total</th>
                                                        </tr>
                                                        @foreach($tripfees as $tripfee)
                                                            <tr>
                                                                <td>{{ $tripfee->created_at }}</td>
                                                                <td>{{ $tripfee->t_type }}</td>
                                                                <td>{{ $tripfee->start_date }}</td>
                                                                <td>{{ $tripfee->end_date }}</td>
                                                                <td>{{ $tripfee->amount_first }}</td>
                                                                <td>{{ $tripfee->amount_second }}</td>
                                                                <td><?php echo (int) $tripfee->amount_first + (int) $tripfee->amount_second ?></td>
                                                            </tr>
                                                        @endforeach
                                                    </table>
                                                </div>
                                            @else
                                                <div class="tab-pane" id="tripfees">
                                                    <div  class="alert alert-success">
                                                        No tripfees.
                                                    </div>
                                                </div>
                                            @endif

                                            @if(count($tickets) > 0)
                                                <div class="tab-pane" id="tickets">
                                                    <table id="example1" class="table table-bordered table-striped">
                                                        <tr>
                                                            <th>Created Date</th>
                                                            <th>Particular</th>
                                                            <th>Amount</th>
                                                        </tr>
                                                        @foreach($tickets as $ticket)
                                                            <tr>
                                                                <td>{{ $ticket->created_at }}</td>
                                                                <td>{{ $ticket->particular }}</td>
                                                                <td>{{ $ticket->amount }}</td>
                                                            </tr>
                                                        @endforeach
                                                    </table>
                                                </div>
                                            @else
                                                <div class="tab-pane" id="tickets">
                                                    <div  class="alert alert-success">
                                                        No tickets.
                                                    </div>
                                                </div>
                                            @endif

                                            @if(count($accidents) > 0)
                                                <div class="tab-pane" id="accidents">
                                                    <table id="example1" class="table table-bordered table-striped">
                                                        <tr>
                                                            <th>Date</th>
                                                            <th>D_name</th>
                                                            <th>Location</th>
                                                            <th>Type</th>
                                                            <th>Details</th>
                                                        </tr>
                                                        @foreach($accidents as $accident)
                                                            <tr>
                                                                <td>{{ $accident->date }}</td>
                                                                <td>{{ $accident->D_name }}</td>
                                                                <td>{{ $accident->location }}</td>
                                                                <td>{{ $accident->type }}</td>
                                                                <td>{{ $accident->details }}</td>
                                                            </tr>
                                                        @endforeach
                                                    </table>
                                                </div>
                                            @else
                                                <div class="tab-pane" id="accidents">
                                                    <div  class="alert alert-success">
                                                        No accidents.
                                                    </div>
                                                </div>
                                            @endif
                                            <!-- /.tab-pane -->
                                        </div>
                                        <!-- /.tab-content -->
                                    </div>
                                    <!-- nav-tabs-custom -->
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
    <style>
        .expired{background: #f4ab90;}
    </style>
@endsection
