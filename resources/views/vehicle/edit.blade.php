@extends("../layout/master")
@include("layout.footer")
@include("layout.header")
@include("layout.meta")
@include("layout.side-bar")

@section("main-content")


    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <section class="content-header">
            <h1>Edit vehicle details</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Vehicle</li>
                <li class="active">Edit</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <form action="{{route('vehicle.update',$vehicles->id)}}" method="post" name="form">
                        {{ csrf_field()}}
                        {{ method_field('PATCH')}}
                        <h2>Add new vehicle</h2>
                        <div class="form-group">
                            <label for="vehicle-number">Number</label>
                            <input id="vehicle-number" class="form-control" type="text"  name="v_no" placeholder="Eg. Lu 2 Pa 2345" value="{{$vehicles->v_no}}" />
                        </div>
                        <div class="form-group">
                            <label for="vehicle-company">Company</label>
                            <input id="vehicle-company" class="form-control" type="text" name="v_company" value="{{$vehicles->company_name }}" placeholder="Eg. Tata, Ford, Suzuki" />
                        </div>
                        <div class="form-group">
                            <label for="vehicle-model">Model</label>
                            <input id="vehicle-model" class="form-control" type="text" value="{{$vehicles->model }}" name="v_model" placeholder="Eg. Hyundai Elite i20" />
                        </div>
                        <div class="form-group">
                            <label for="vehicle-model">Chassis Number</label>
                            <input id="chassis_no" class="form-control" type="text" value="{{$vehicles->chassis_no }}" name="chassis_no" placeholder="Eg. ss34-53-42" />
                        </div>
                        <div class="form-group">
                            <label for="vehicle-type">Type</label>
                            <input id="vehicle-type" class="form-control" type="text" name="v_type" value="{{$vehicles->v_type }}" placeholder="Eg. Jeep, Mini-Bus, Micro-Bus" />
                        </div>
                        <div class="form-horizontal row">
                            <label class="col-lg-12">Insurance</label>
                            <div class="form-group col-md-6">
                                <label for="vehicle-route-from" class="col-sm-2 control-label">From<span class="astrict">*</span> </label>
                                @if($errors->has('date_from'))
                                    <span class="error">  {{  $errors->first('date_from')}}</span>
                                @endif
                                <div class="input-group date">

                                    <div class="input-group-addon" >
                                        <i class="fa fa-calendar"></i>
                                    </div>

                                    <input type="text" class="form-control pull-right" id="from" name="date_from" value="{{ $vehicles->date_from }}">

                                </div>
                                <!-- /.input group -->
                            </div>
                            <div class="form-group col-md-6">
                                <label for="vehicle-route-from" class="col-sm-2 control-label">To<span class="astrict">*</span> </label>
                                @if($errors->has('date_to'))
                                    <span class="error">  {{  $errors->first('date_to')}}</span>
                                @endif
                                <div class="input-group date">

                                    <div class="input-group-addon" >
                                        <i class="fa fa-calendar"></i>
                                    </div>

                                    <input type="text" class="form-control pull-right" id="to" name="date_to" value="{{ $vehicles->date_to }}">

                                </div>
                                <!-- /.input group -->
                            </div>

                        </div>
                        <div class="form-horizontal">
                            <label>Route</label>
                            <div class="form-group">
                                <label for="vehicle-route-from" class="col-sm-1 control-label">From</label>
                                <div class="col-sm-11">
                                    <input type="text" class="form-control" id="vehicle-route-from" value="{{$vehicles->route_from }}" placeholder="Eg. Butwal" name="from">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="vehicle-route-to" class="col-sm-1 control-label">To</label>

                                <div class="col-sm-11">
                                    <input type="text" class="form-control" id="vehicle-route-to" value="{{$vehicles->route_to }}" placeholder="Eg. Kathmandu" name="to">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" name="submit-vehicle" value="Submit" />

                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
    <script>
        $(function () {




            $("#from").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayBtn: "linked",
                endDate:'today',
                todayHighlight: true,
            }).on('changeDate', function (selected) {
                var startDate = new Date(selected.date.valueOf());
                $('#to').datepicker('setStartDate', startDate);
            }).on('clearDate', function (selected) {
                $('#to').datepicker('setStartDate', null);
            });

            $("#to").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayBtn: "linked",

                todayHighlight: true,
            }).on('changeDate', function (selected) {
                var endDate = new Date(selected.date.valueOf());
                $('#from').datepicker('setEndDate', endDate);
            }).on('clearDate', function (selected) {
                $('#from').datepicker('setEndDate', null);

            });

        });
    </script>


@endsection
