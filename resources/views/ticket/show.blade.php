@extends("../layout/master")
@include("layout.footer")
@include("layout.header")
@include("layout.meta")
@include("layout.side-bar")

@section("main-content")
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Tickets
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Ticket</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                    {{--<div class="box-header">
                        <h3 class="box-title">Data Table With Full Features</h3>
                    </div>--}}
                    <!-- /.box-header -->
                        <div class="box-body">
                            @if(Session::has('message'))
                                <p class="message"></p>
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h4><i class="icon fa fa-check"></i> Success</h4>
                                    {{ Session::get('message') }}
                                </div>
                            @endif
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>S.N</th>
                                    <th>Particular</th>
                                    <th>Name</th>
                                    <th>Amount</th>
                                     <th>Date</th>
                                    <th>Vehicle No.</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($tickets as $ticket)
                                    <tr>
                                        <td>{{$loop->index+1 }}</td>
                                        <td>{{ $ticket->particular }}</td>
                                        <td>{{$ticket->name}}</td>
                                        <td>{{$ticket->amount}}</td>
                                        <td>{{$ticket->date}}</td>
                                        <td>{{$ticket->v_no}}</td>
                                        <td>
                                          @php $type= Auth::user()->user_type; @endphp
                                          @if($type=='admin')

                                            <form id="delete-item{{$ticket->id}}"  action="{{route('ticket.destroy',$ticket->id)}}" style="Display:none"  method="post">
                                                {{csrf_field()}}
                                                {{method_field('DELETE')}}

                                            </form>
                                            <a href="" onclick="
                                                    if (confirm('Are you sure, You want to delete this ?'))
                                                    {
                                                    event.preventDefault();
                                                    document.getElementById('delete-item{{$ticket->id}}').submit();
                                                    }
                                                    else{
                                                    event.preventDefault();
                                                    }

                                                    ">

                                                <i class="fa fa-fw fa-trash"></i>||
                                                     @endif
                                                <a href="{{ route('ticket.show',$ticket->id) }}" target="_blank"><i class="fa fa-print" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        No data Are Found
                                    </tr>
                                @endforelse
                                </tbody>
                                {{--<tfoot>
                                <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                                </tr>
                                </tfoot>--}}
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
    </div>
    <script>
        $(function () {
            $('#example1').DataTable();
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            });
        })
    </script>
@endsection
