@extends("../layout/master")
@include("layout.footer")
@include("layout.header")
@include("layout.meta")
@include("layout.side-bar")

@section("main-content")

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add new ticket
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Ticket</a></li>
                <li><a href="#">Add</a></li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                    {{--<div class="box-header">
                        <h3 class="box-title">Data Table With Full Features</h3>
                    </div>--}}
                    <!-- /.box-header -->
                        <div class="box-body">
            <form action="{{route('ticket.store')}}" method="POST">
                <div class="form-group">
                    <label>Vehicle Number</label>

                    {{--@if($errors->has('v_no'))
                        <span class="error">  *{{  $errors->first('v_no')}}</span>
                    @endif--}}

                    <select class="form-control select2 select2-hidden-accessible" name="v_no" id="v_number" style="width: 100%;">
                        
                        <option value="" >Select Vehicle No</option>
                        @forelse($vehicles as $vehicle)
                            <option value="{{$vehicle->v_no}}">{{$vehicle->v_no}}</option>
                        @empty
                            <option>Not Found</option>
                        @endforelse
                    </select>
                </div>
                <div class="form-group">
                    <label for="particular">Particular</label>
                    @if($errors->has('particular'))
                        <span class="error">  *{{  $errors->first('particular')}}</span>
                    @endif

                    <select class="form-control particular-select" name="particular" id="particular" >
                        style="width: 100%;">
                        <option value="">Select your subject.</option>
                        @forelse($tickets as $ticket)
                            <option value="{{$ticket->particular}}">{{$ticket->particular}}</option>
                        @empty
                            <option disabled>Not Found</option>
                        @endforelse
                    </select>
                    <input id="particular" class="form-control particular-text" type="text" name="particular" placeholder="Eg. Application" disabled />

                    <button style="margin:10px 0;" type="button" class="btn btn-default" id="add-new-btn">Add New</button>
                    <button style="margin:10px 0;" type="button" class="btn btn-default" id="choose-existing-btn">Choose Existing</button>
                </div>
                <div class="form-group">
                    <label for="name">Name</label>

                    @if($errors->has('name'))
                        <span class="error">  *{{  $errors->first('name')}}</span>
                    @endif

                    <input id="name" class="form-control" type="text" name="name" placeholder="Eg.Mohan Rana Chhetri" />

                </div>
                <div class="form-group">
                    <label for="amount">Amount</label>
                    @if($errors->has('amount'))
                        <span class="error">  *{{  $errors->first('amount')}}</span>
                    @endif
                    <input id="amount" class="form-control" type="text" name="amount" placeholder="Eg. 10" />
                </div>
                <div class="form-group">
                    <label for="date">Date</label>
                    @if($errors->has('date'))
                        <span class="error">  {{  $errors->first('date')}}</span>
                    @endif
                    <div class="input-group date">

                        <div class="input-group-addon" >
                            <i class="fa fa-calendar"></i>
                        </div>

                        <input type="text" class="form-control pull-right" id="from" name="date">

                    </div>
                    <!-- /.input group -->
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" name="submit-owner" value="Submit" />
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </div>
            </form>
                        </div></div></div></div>
        </section>
    </div>
    <script>
        $(function () {


            $('.select2').select2();

            $("#from").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayBtn: "linked",
                endDate:'today',
                startDate:'today',
                todayHighlight: true,
            }).on('changeDate', function (selected) {
                var startDate = new Date(selected.date.valueOf());
                $('#to').datepicker('setStartDate', startDate);
            }).on('clearDate', function (selected) {
                $('#to').datepicker('setStartDate', null);
            });



        });
    </script>
    <style>
        .particular-text,#choose-existing-btn{display:none;}
    </style>
    <script>
        $("#add-new-btn").click(function(){
            $('.particular-select').hide();
            $('.particular-select').prop('disabled','true');
            $('.particular-text').show();
            $('.particular-text').removeAttr('disabled');
            $('#choose-existing-btn').show();
            $('#add-new-btn').hide();
        });
        $("#choose-existing-btn").click(function(){
            $('.particular-select').show();
            $('.particular-select').removeAttr('disabled');
            $('.particular-text').hide();
            $('.particular-text').prop('disabled','true');
            $('#choose-existing-btn').hide();
            $('#add-new-btn').show();
        });
    </script>

@endsection
