<!DOCTYPE html>

<html lang="en-US">

<head>
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}" />
</head>

<body>
<div class="container" style="border: 2px solid black;padding: 10px;">
    <main>
        <div class="sec1">
            <p class="left"><span class="bold">रजिष्टड</span> 101/091/092</p>
            <p class="right"><span class="bold">फोन नं.</span> 071-541105</p>
        </div>
        <div class="sec2">
            <div class="img-sec">
                <img class="logo-img" src="{{ asset('images/new_butwal_yatayat_logo.png') }}" alt="Butwal Yatayat" />
            </div>
            <div class="sec3">
                <h1>बुटवल यातायात व्यवसायि समिति</h1>
                <p class="small">प्रधान कार्यलय बुटवल-७</p>
                <p class="small">नगद रसिद</p>
            </div>
            <div class="sec4">
                <p>रसिद नं:     {{ $tripfee['id'] }}</p>
                <p>मिति:         {{$tripfee['date_and_time_np']}} <br> {{$tripfee['date_and_time']}} </p>

            </div>
        </div>
        <div class="sec6">
            <p><span class="bold">गाडी नं:</span>     {{$tripfee['v_no']}}</p>
            <p><span class="bold">गाडी धनिको नाम:</span>   {{$owner['name']}}</p>
            <p><span class="bold">ठेगाना:</span>         {{$owner['address']}}</p>
            <p><span class="bold">फोन नं:</span>       {{$owner['contact_no']}}</p>
        </div>
        <div class="sec7">
            <p><span class="bold">चालको नाम:</span>          {{$driver['name']}}</p>
            <p><span class="bold">ठेगाना:</span>         {{$driver['address']}}</p>
            <p><span class="bold">फोन नं:</span>       {{$driver['contact_no']}}</p>
        </div>
        <div class="sec8">
            <table>
                <thead>
                <tr>
                    <th>विवरण</th>
                    <th>दैनिक रू.</th>
                    <th>मासिक रू.</th>
                    <th>अर्ध-मासिक रू.</th>
                    <th>देखि</th>
                    <th>सम्म</th>
                    <th>कैफियत</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>समिति ट्रिप शुल्क</td>
                    <?php if($tripfee['type']=='daily'){echo "<td>".$tripfee['amount_first']."</td><td></td><td></td>";}?>
                    <?php if($tripfee['type']=='monthly'){echo "<td></td><td>".$tripfee['amount_first']."</td><td></td>";}?>
                    <?php if($tripfee['type']=='semi_monthly'){echo "<td></td><td></td><td>".$tripfee['amount_first']."</td>";}?>
                    <td rowspan="3">{{ $tripfee['start_date'] }}</td>
                    <td rowspan="3">{{ $tripfee['end_date'] }}</td>
                    <td>-</td>
                </tr>
                <tr>
                    <td>गामा संरक्षण शुल्क</td>
                    <?php if($tripfee['type']=='daily'){echo "<td>".$tripfee['amount_second']."</td><td></td><td></td>";}?>
                    <?php if($tripfee['type']=='monthly'){echo "<td></td><td>".$tripfee['amount_second']."</td><td></td>";}?>
                    <?php if($tripfee['type']=='semi_monthly'){echo "<td></td><td></td><td>".$tripfee['amount_second']."</td>";}?>
                    <td>-</td>
                </tr>
                <tr>
                    <td>दुर्घटना सहयोगि कोष शुल्क(जम्मा रू)</td>
                    <?php if($tripfee['type']=='daily'){echo "<td>".$tripfee['total']."</td><td></td><td></td>";}?>
                    <?php if($tripfee['type']=='monthly'){echo "<td></td><td>".$tripfee['total']."</td><td></td>";}?>
                    <?php if($tripfee['type']=='semi_monthly'){echo "<td></td><td></td><td>".$tripfee['total']."</td>";}?>
                    <td>-</td>
                </tr>
                </tbody>
            </table>
        </div>
    </main>
    <footer>
        <div class="sec9">
            <p><span class="bold">नोट</span></p>
            <ol>
                <li>यो रसिद सुरक्षित साथ राख्नुहोला ।</li>
                <li>दुर्घटना हुन साथ तुरुन्त समितिमा खबर गरौ ।</li>
            </ol>
        </div>
        <div class="sec10">
            <p>.....................</p>
            <p><span class="bold">बुझाउनेको सहि</span></p>
        </div>
        <div class="sec11">
            <p>.....................</p>
            <p><span class="bold">बुझ्नेको सहि</span></p>
        </div>
    </footer>


        <button type="button" onclick="window.print()" class="btn btn-default print-btn">print</button>

</div>
</body>

</html>
