<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/manage','HomeController@index')->name('manage');

Route::get('/backup','BackupController@create')->name('backup');

Route::get('/logout', 'Auth\LoginController@logout');

Route::post('/report',[
    'uses'=>'HomeController@report',
    'as'=>'report'
    ]);
Route::post('/insurancereport',[
    'uses'=>'HomeController@insurancereport',
    'as'=>'reports'
]);

Route::post('/ticketReport',[
    'uses'=>'HomeController@ticketReport',
    'as'=>'reportss'
]);
Route::get('/export',[
    'uses'=>'HomeController@export',
    'as'=>'export'
]);
Route::get('/exportInsurance',[
    'uses'=>'HomeController@exportInsurance',
    'as'=>'exportInsurance'
]);
Route::get('/ticketReportPrint',[
    'uses'=>'HomeController@ticketReportPrint',
    'as'=>'ticketReportPrint'
]);
Route::resource('vehicle','VehicleController');
Route::resource('owner','OwnerController');
Route::resource('driver','DriverController');
Route::resource('accident','AccidentController');
Route::resource('tripfee','Tripfeecontroller');
Route::resource('ticket','TicketController');

Auth::routes();
